import { Component } from '@angular/core';

@Component({
  selector: 'body[hpl-root]',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hpl-admin';
}
