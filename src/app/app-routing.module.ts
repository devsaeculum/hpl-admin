import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationGuardService } from './core/guards/authorization-guard.service';
import { BaseLayoutComponent } from './modules/layout/components/base-layout/base-layout.component';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/login/login.module')
      .then(mod => mod.LoginModule),
  },
  {
    path: 'user-list',
    loadChildren: () => import('./modules/user-list/user-list.module')
      .then(mod => mod.UserListModule),
    component: BaseLayoutComponent,
    canActivate: [AuthorizationGuardService],
    canActivateChild: [AuthorizationGuardService]
  },
  {
    path: 'hamburger-menu',
    loadChildren: () => import('./modules/hamburger-menu/hamburger-menu.module')
      .then(mod => mod.HamburgerMenuModule),
    component: BaseLayoutComponent,
    canActivate: [AuthorizationGuardService],
    canActivateChild: [AuthorizationGuardService]
  },
  { path: '', pathMatch: 'full', redirectTo: '/auth' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
  })],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule { }
