// ANGULAR DEPENDENCY
import { NgModule } from '@angular/core';

// NGRX
import { EffectsModule } from '@ngrx/effects';
import { ActionReducer, MetaReducer, StoreModule } from '@ngrx/store';

// STORE REDUCER AND EFFECTS
import { StateStoreService } from './state-store.service';
import { StoreEffects } from './store/effects';
import * as fromJob from './store/reducer';


// add localstorage function in meta reducers
const metaReducers: Array<MetaReducer<any, any>> = [];


@NgModule({
  imports: [  
    StoreModule.forRoot({ router: null }, { metaReducers }),
    EffectsModule.forRoot([StoreEffects]),
  ],
  declarations: [],
  providers: [
    StateStoreService,
  ],
})
export class StateStoreModule { }
