// ANGULAR DEPENDENCY
import { Injectable } from '@angular/core';

// NGRX AND RXJS
import { Store } from '@ngrx/store';

// GLOBAL ACTIONS, SELECTORS AND STATES
import { State } from './root-state';
import * as actions from './store/actions';
import * as selectors from './store/selectors';

// COMMON INTERFACES
import { Observable } from 'rxjs';


@Injectable()
export class StateStoreService {

  constructor(
    protected store: Store<State>,
  ) {
  }

  dispatchJobCommon(payload: any) {
    ('call STORE SERVICE');
    this.store.dispatch(new actions.StateAction(payload));
  }
  /************** Direct Api Call ************** */


}
