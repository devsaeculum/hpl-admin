// STATES
import { StoreState } from './store/state';


export interface State {
    storeState: StoreState;
}
