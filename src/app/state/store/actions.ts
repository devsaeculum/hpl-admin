// STORE DEPENDANCY
import { Action } from '@ngrx/store';

export enum ActionTypes {
  // PROJECT ACTIONS
  STATE = '[Global] Get State',
  STATE_SUCCESS = '[Global] Get State Success',
}

// -------------------------------
//  State Get
// -------------------------------
export class StateAction implements Action {
  readonly type = ActionTypes.STATE;
  constructor(public payload: any) { }
}

// -------------------------------
//  State Get Success
// -------------------------------
export class StateSuccessAction implements Action {
  readonly type = ActionTypes.STATE_SUCCESS;
  constructor(public response?: any) { }
}

export type Actions =
  | StateAction
  | StateSuccessAction;

