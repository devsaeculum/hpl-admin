// STORE ACTION AND STATE
import { Actions, ActionTypes } from './actions';
import { initialState, StoreState } from './state';

export function reducer(state = initialState, action: Actions): StoreState {
    switch (action.type) {
        // GET_JOB_COMMON METHOD
        case ActionTypes.STATE: {
            return state;
        }

        // GET_JOB_SUCCESS COMMON METHOD
        case ActionTypes.STATE_SUCCESS: {
            ('STATE_SUCCESS ::>',action)
            // commonly store and handle all master modules
            return {
                ...state,
                [action.response.common.method]: action.response.data,
            };
        }
        default:
            return state;
    }
}
