// NGRX STORE
import { createFeatureSelector, createSelector, MemoizedSelector } from '@ngrx/store';

// STORE STATE
import { StoreState } from './state';


export const selectState: MemoizedSelector<
  object,
  StoreState
> = createFeatureSelector<StoreState>('STORE');