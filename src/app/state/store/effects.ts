// ANGULAR
import { Injectable } from '@angular/core';

// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

// RXJS
import { Observable, of } from 'rxjs';
import { catchError, mergeMap, withLatestFrom } from 'rxjs/operators';
import { UserService } from 'src/app/modules/services/user.service';

// SERVICES

// ACTIONS AND STATES
// import * as StateActions from '../../global/store/actions';

import { State } from '../root-state';
import * as storeActions from './actions';

// COMMON INTERFACES AND ENUMS
// import { ModalType } from '../../../models/common/modal/modal-config';

@Injectable()
export class StoreEffects {
    constructor(
        private actions$: Actions,
        private store: Store<State>,
        private userService: UserService) { }

    /**
        * Project Effect
        * @param type : GET_PROJECT action name.
        * @param payload : object to identify common project method base on module and method.
        * @returns Empty action dispatch to reducer.
        */
    @Effect()
    storeEffect$: Observable<any> = this.actions$.pipe(
        ofType<storeActions.StateAction>(storeActions.ActionTypes.STATE),
        withLatestFrom(this.store),
        mergeMap(([action, state]): any => {
            return this[action.payload.service][action.payload.method](action.payload.data)
                .pipe(
                    mergeMap((data) => {
                        let actions: any[] = [];

                        // dispatch project success action
                        actions = [new storeActions.StateSuccessAction({ data, common: action.payload })];

                        // dispatch popup notification for open success popup.
                        ('EFFECT PAYLOAD ::>',action.payload);
                        if (action.payload.successNotification) {
                            //   actions.push(new globalActions.PopupNotificationAction({
                            //     actionType: storeActions.ActionTypes.PROJECT, type: ModalType.SUCCESS,
                            //     message: common.SUCCESS, callback: action.payload.callback, title: common.TITLE,
                            //   }));
                        } else {
                            // tslint:disable-next-line:no-unused-expression
                            action.payload.callback && action.payload.callback({ success: true, data });
                        }

                        return actions;
                    }),
                    catchError(error => {
                        (error);
                        // dispatch popup notification for open error popup
                        return of();
                        // return of(new globalActions.PopupNotificationAction({
                        //   actionType: storeActions.ActionTypes.PROJECT, type: ModalType.ERROR,
                        //   message: error.error.message, callback: action.payload.callback, error: error.error,
                        //   title: common.TITLE,
                        // }))
                    }
                    ));
        }),
    );

}
