// NGRX
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';


// -------------------------------
//  Project State
// -------------------------------
export interface StoreState extends EntityState<any> {
    storeState?: any;
}

export const masterAdapter: EntityAdapter<any> = createEntityAdapter<any>({});
export const initialState: StoreState = masterAdapter.getInitialState(
    {
        storeState: null,
    },
);