import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss', './../login/login.component.scss']
})
export class ForgotPasswordComponent implements OnInit {

  forgetPasswordForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
  });

  submitted = false;

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  onSubmit() {
    this.submitted = true;
    if (this.forgetPasswordForm.invalid) {
      Object.keys(this.forgetPasswordForm.controls).forEach(controlName =>
        this.forgetPasswordForm.controls[controlName].markAsTouched()
      )
    }else{
      this.router.navigate(['./auth']);
    }
  
  }

  onCancle() {
    this.router.navigate(['./auth']);
  }

  onForgotPassword() {
    this.router.navigate(['./auth']);
  }

}
