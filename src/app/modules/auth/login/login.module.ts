// ANGULAR
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// TRANSLATE
import { TranslateModule } from '@ngx-translate/core';
import { AuthService } from '../../services/auth/auth.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';
import { LoginRoutingModule } from './login-routing.module';


// COMPONENTS
import { LoginComponent } from './login.component';

/**
 * This module used for handle user login
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) login module created
 */
@NgModule({
  declarations: [LoginComponent , ForgotPasswordComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LoginRoutingModule,
    HttpClientModule,
    /********************EXTERNAL MODULES*******************/
    TranslateModule.forChild(),
    /********************CUSTOM MODULES*******************/
  ],
    providers : [
    AuthService,

    ]
})
export class LoginModule { }
