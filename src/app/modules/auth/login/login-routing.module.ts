import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginAuthorizationGuardService } from 'src/app/core/guards/login-authorization-guard.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

import { LoginComponent } from './login.component';

const routes: Routes = [
  { path: '', component: LoginComponent, canActivate: [LoginAuthorizationGuardService] },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: '', pathMatch: 'full', redirectTo: '/auth' },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
