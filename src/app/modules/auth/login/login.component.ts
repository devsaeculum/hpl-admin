// Angular
import { Component, OnDestroy, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

// RxJS
import { Subscription } from 'rxjs';

// CONSTANTS
import { NgxPermissionsService } from 'ngx-permissions';
import { AuthStorageService } from '../../services/auth/auth-storage.service';
import { AuthService } from '../../services/auth/auth.service';
import { ERROR } from 'src/app/core/constants/error-code.constant';



/**
 * This component used for user login
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) intial login component created
 */
@Component({
  selector: 'kt-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit, OnDestroy, AfterViewInit {
  /**
   * Constants
   */
  /**
   * Button config
   */
  /**
   * login form group declare
   */
  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
    passWord: new FormControl('', [Validators.required,Validators.minLength(8)]),
  });

  submitted = false;


  /**
   * userful other variables
   */
  loading = false;
  private returnUrl: any;
  /**
   * Subscriptions handle on destroy
   */
  subscriptions: Subscription[] = [];


  constructor(
    private router: Router,
    private permissionService: NgxPermissionsService,
    private authenticationService: AuthService,
    private authStorageService: AuthStorageService,
  ) {
  }

  /**
     * Lifecycle hook called to initailize the component
     */
  ngOnInit(): void {
  }

  ngAfterViewInit() {
  }

  /**
   * Get BrowserData
   */
  getBrowserInfo() {
  }

  /**
   * Submit Login form
   */
  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      Object.keys(this.loginForm.controls).forEach(controlName =>
        this.loginForm.controls[controlName].markAsTouched()
      )
    }
    else {
      const payload = {
        process: 'mobile_login',
        client_data: {
          enc: { value: 9662507875 },
          b64: { value: 9662507875 },
          silent_user_id: { value: '9662507875' },
          hardware_id: { value: '9662507897' },
          username: { value: '132456789' },
          affiliate_id: { value: 9662507875 }
        },
        method: 'signup_login'
      };
      this.authenticationService.signupLogin(payload).subscribe((response: any) => {
        if (response && response.result && response.result.code === ERROR.OK) {
          this.router.navigate(['/user-list']);
        }
      });
      this.submitted = false;
    }

  }

  /**
   * Popup Notification Callback Function.
   * @return callback - returns callback of popup notification.
   */
  getCallBack(data) {
  }

  /**
   * Destroy all subscriptions
   */
  ngOnDestroy(): void {
  }

  // sendCode() {
  //   if (this.loginForm.invalid) {
  //     this.loginForm.markAsTouched();
  //   }
  //   if (this.loginForm.valid) {
  //     const payload = {
  //       process: 'mobile_login',
  //       client_data: {
  //         enc: { value: 9662507875 },
  //         b64: { value: 9662507875 },
  //         silent_user_id: { value: '9662507875' },
  //         hardware_id: { value: '9662507897' },
  //         username: { value: '132456789' },
  //         affiliate_id: { value: 9662507875 }
  //       },
  //       method: 'signup_login'
  //     };
  //     this.authenticationService.signupLogin(payload).subscribe((response: any) => {
  //       if (response && response.result && response.result.code === ERROR.OK) {
  //         this.router.navigate(['/user-list']);
  //       }
  //     });
  //   }
  // }
}
