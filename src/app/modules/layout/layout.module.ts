import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { BaseLayoutComponent } from './components/base-layout/base-layout.component';

import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MenuConfigService } from 'src/app/core/services/menu-config.service';
import { MenuHorizontalService } from 'src/app/core';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    BaseLayoutComponent,

  ],
  imports: [
    NgbModule,
    CommonModule,
    RouterModule,
  ],
  providers: [
    MenuConfigService,
    MenuHorizontalService
  ]
})
export class LayoutModule { }
