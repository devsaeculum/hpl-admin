import { Component, OnInit, HostListener } from '@angular/core';
import { from, fromEvent, Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, tap } from 'rxjs/operators';
import { MenuConfigService } from '../../../../core/services/menu-config.service';

import { MenuConfig } from '../../../../core/constants/header-config.constant';


@Component({
  selector: 'app-base-layout',
  templateUrl: './base-layout.component.html',
  styleUrls: ['./base-layout.component.scss']
})
export class BaseLayoutComponent implements OnInit {
  constructor(private menuConfigService: MenuConfigService) {
    this.menuConfigService.loadConfigs(new MenuConfig().configs);

  }

  ngOnInit() {
  }



  ngOnDestroy() {
  }

}
