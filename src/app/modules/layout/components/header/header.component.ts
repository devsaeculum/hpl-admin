import { Component, OnDestroy, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalContainerBaseClass } from '../../../shared/components/modals/_base-classes/modal-container-base-class';
import { Subscription } from 'rxjs';
import { ModalBaseClass } from 'src/app/modules/shared/components/modals/base_class/modal-base-class';
import { ChangePasswordComponent } from 'src/app/modules/user-list/change-password/change-password.component';
import { MyProfileComponent } from 'src/app/modules/user-list/my-profile/my-profile.component';
import { MenuHorizontalService } from '../../../../core/services/menu-horizontal.service';
import { filter } from 'rxjs/operators';

// Object-Path
import * as objectPath from 'object-path';


@Component({
  selector: 'kt-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends ModalBaseClass implements OnInit, OnDestroy {

  user: any = {};
  currentRouteUrl: any = '';
  // subscribe
  subscriptions: Subscription[] = [];
  defaultLogo = 'assets/icons/user.svg';

  //header logo
  fluid: boolean;
  headerLogo: string;

  constructor(
    public router: Router, public modalservice: NgbModal,
    public menuHorService: MenuHorizontalService,
    private cdr: ChangeDetectorRef,
  ) {
    super(modalservice);
  }

  ngOnInit() {
    this.currentRouteUrl = this.router.url;
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => {
        this.currentRouteUrl = this.router.url;
        this.cdr.markForCheck();
      });
    //  (this.currentRouteUrl, this.router.url);
  }

  userProfile() {
    // ('Call');
    // this.router.navigateByUrl('/', { skipLocationChange: true }).then(() =>
    //   this.router.navigate(['/user/user-profile']));
  }

  /**
   * Log out
   */
  logout() {
    localStorage.clear();
    this.router.navigate(['/auth']);
  }

  myProfileClickHandler() {
    const modalRef = this.openPopUp(MyProfileComponent, null, { centered: true, size: 'md', keyboard: true, backdrop: 'static' });
  }

  changePasswordHandler() {
    const modalRef = this.openPopUp(ChangePasswordComponent, null, { centered: true, size: 'md', keyboard: true, backdrop: 'static' });
  }

  ngOnDestroy() {

  }

  getItemAttrSubmenuToggle(item) {
    // (item);

    let toggle = 'hover';
    if (objectPath.get(item, 'toggle') === 'click') {
      toggle = 'click';
    } else if (objectPath.get(item, 'submenu.type') === 'tabs') {
      toggle = 'tabs';
    } else {
      // submenu toggle default to 'hover'
    }
    // (toggle);

    return toggle;
  }

  getItemCssClasses(item) {
    let classes = 'kt-menu__item';

    if (objectPath.get(item, 'submenu')) {
      classes += ' kt-menu__item--submenu';
    }

    if (!item.submenu && this.isMenuItemIsActive(item)) {
      classes += ' kt-menu__item--active kt-menu__item--here';
    }

    if (item.submenu && this.isMenuItemIsActive(item)) {
      classes += ' kt-menu__item--open kt-menu__item--here';
    }

    if (objectPath.get(item, 'resizer')) {
      classes += ' kt-menu__item--resize';
    }

    const menuType = objectPath.get(item, 'submenu.type') || 'classic';
    if ((objectPath.get(item, 'root') && menuType === 'classic')
      || parseInt(objectPath.get(item, 'submenu.width'), 10) > 0) {
      classes += ' kt-menu__item--rel';
    }

    const customClass = objectPath.get(item, 'custom-class');
    if (customClass) {
      classes += ' ' + customClass;
    }

    if (objectPath.get(item, 'icon-only')) {
      classes += ' kt-menu__item--icon-only';
    }

    return classes;
  }

  isMenuItemIsActive(item): boolean {
    // (item);
    // if (item.submenu) {
    //   return this.isMenuRootItemIsActive(item);
    // }

    if (!item.page) {
      return false;
    }

    if (item.queryParams) {
      let hasQueryParams = true;
      Object.keys(item.queryParams).forEach(key => {
        if (!this.currentRouteUrl.includes(item.queryParams[key])) {
          if (hasQueryParams) {
            hasQueryParams = false;
          }
        }
      });
      return this.currentRouteUrl.indexOf(item.page) !== -1 && hasQueryParams;
    } else {
      return this.currentRouteUrl.indexOf(item.page) !== -1;
    }
  }

  isMenuRootItemIsActive(item): boolean {
    if (item.submenu.items) {
      for (const subItem of item.submenu.items) {
        if (this.isMenuItemIsActive(subItem)) {
          return true;
        }
      }
    }

    if (item.submenu.columns) {
      for (const subItem of item.submenu.columns) {
        if (this.isMenuItemIsActive(subItem)) {
          return true;
        }
      }
    }

    if (typeof item.submenu[Symbol.iterator] === 'function') {
      for (const subItem of item.submenu) {
        const active = this.isMenuItemIsActive(subItem);
        if (active) {
          return true;
        }
      }
    }

    return false;
  }

}
