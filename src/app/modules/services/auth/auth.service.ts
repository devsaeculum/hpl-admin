import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthStorageService } from './auth-storage.service';

import { tap } from 'rxjs/operators';
import { ERROR } from '../../../core/constants/error-code.constant';
// import { SocialAuthService } from 'angularx-social-login';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userDetails = ['first_name', 'gender', 'last_name', 'new_user', 'real_user', 'user_id', 'user_name'];

  constructor(
    private http: HttpClient,
    public authStoreService: AuthStorageService,
    // private socialAuthService: SocialAuthService
    ) 
    { }

  getUserDetails(res) {
    const details = {};
    this.userDetails.map(key => details[key] = res[key]);
    return details;
  }

  public saveAccessData(accessData) {
    const res = accessData && accessData.result.data;
    if (res) {
      this.authStoreService
        .setSessionSecret(res.gametoken)
        .setSessionToken(res.gametoken)
        .setUserDetails(this.getUserDetails(res));
    }
    return this;
  }

  silentLogin(payload) {
    return this.http.post(environment.hungamaApiUrl, payload).pipe(
      tap(n => this.saveAccessData(this.handleError(n)))
    );
  }

  signupLogin(payload) {
    const res = { "result": { "code": 200, "data": { "real_user": "true", "user_id": "609243081", "gigya_login_session_token": "st2.s.AcbHG54wew.PqNvVO6BegWcjPeqkmvKM8_IvFPoPqvMshDa0vPoYMcIBpSN6gAymCEUCJGEHojwYzUykaQJdZGq-zysKDYw9fwmNwQ2CKx3JhNieIY03IA.ZRZxP7OFnohKoF0BlTRDtwy7-R1x_ZLQAyw3nQ1rRdfR8TAIJNXcmLkNKAAS4huh3exw5be4r_c6ypIfPSUlOw.sc3", "gigya_login_session_secret": "iSshjjoR1W5YJu9CyrM1ROvrU+Q=", "new_user": "false", "first_name": "", "last_name": "", "user_name": "132456789", "gametoken": "b%2BUbguSs2rzpcJE29OkZuZxlkGM0lQbMhzauabguO4MP1MDw5mPuPGqzQbtnRGYHliXj5j11H7O5O3HB40Kaef8DtU%2Fvqv0CfiHw%2FA9Ku2jqd9sFQjxCur%2B9PHT%2BTsHHJ276xQ4Lwa3hNFHmYPy5ErBXCD4iQ8Ny4jM2S6gpJeAfgH9nuyEV8%2Bk55RnnwKWF", "gender": "", "doy": "" }, "message": "" }, "error": "null" }
    this.saveAccessData(this.handleError(res));
    return of(res);
  }

  handleError(res) {
    if (res && res.result && res.result.code === ERROR.FAIL) {
      // this.snackbar.open(res.result.message, '', {
      //   duration: 3000,
      //   panelClass: ['error'],
      // });
      return null;
    }
    return res;
  }
}
