import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthStorageService {

  constructor() { }

  public isAuthorized(): Observable<boolean> {
    const sessionTokenDate = this.getSessionToken();
    const auth: any = {
      isLoggedIn: !sessionTokenDate ? false : true,
    };
    return of(auth);
  }

  /**
   * Set refresh token
   * @returns AuthStorageService
   */
  public setSessionSecret(secret: string): AuthStorageService {
    localStorage.setItem('sessionSecret', secret);
    return this;
  }

  /**
   * Set refresh token
   * @returns AuthStorageService
   */
  public getSessionSecret(): string {
    const sessionSecret = localStorage.getItem('sessionSecret') as string;
    return sessionSecret;
  }

  /**
   * Set refresh token
   * @returns AuthStorageService
   */
  public setSessionToken(token: string): AuthStorageService {
    localStorage.setItem('sessionToken', token);
    return this;
  }

  /**
   * Get refresh token
   * @returns AuthStorageService
   */
  public getSessionToken(): string {
    const sessionToken = localStorage.getItem('sessionToken') as string;
    return sessionToken;
  }

  /**
   * Set user data
   * @returns AuthStorageService
   */
  public setUserDetails(userDetails: any): AuthStorageService {
    localStorage.setItem('userDetails', JSON.stringify(userDetails));
    return this;
  }

  /**
   * Get user data
   * @returns AuthStorageService
   */
  public getUserDetails(): any {
    const userDetails = localStorage.getItem('userDetails') && JSON.parse(localStorage.getItem('userDetails'));
    return userDetails;
  }

  public logout() {
    localStorage.clear();
    return true;
  }

}
