// Interface And Enum
import { ActionButtonType, ButtonConfigType, ButtonType } from '../../models/common/forms/button';
import { DisplayType, ExportActionType, ExportType, FilterType, ModeType, ResizeType, TableConfig } from '../../models/common/table/table-config';

// Constant
import { BACK_BUTTON_CONSTANT } from '../../constants/translate-message/button';
import { ControlType } from '../../models/common/forms/form-field-config';
import { TemplateFormComponentType } from '../../models/common/forms/template-driven';
/**
 * Table Config for the Employee Component
 */
export const CREATE_CLIENT_AREA_TABLE_CONSTANT: TableConfig = {
    rows: 10,
    paginator: true,
    lazy: false,
    sortMode: ModeType.SINGLE,
    reorderableColumns: true,
    resizableColumns: true,
    scrollable: true,
    scrollHeight: 'calc(100vh - 180px)',
    fullPageHeight: true,
    responsive: false,
    reorderableRows: false,
    dataKey: 'employee_id',
    customSort: false,
    selectionMode: ModeType.SINGLE,
    checkboxSelection: false,
    stateKey: '',
    columnResizeMode: ResizeType.EXPAND,
    filterable: true,
    columns: [
        {
            field: 'user_name',
            header: 'First Name',
            sortable: true,
            filterable: true,
            filterType: FilterType.STRING,
            setting: {
                key: 'user_name',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'First Name',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_last_name',
            header: 'Last Name',
            sortable: true,
            filterable: true,
            filterType: FilterType.STRING,
            setting: {
                key: 'user_last_name',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'Last Name',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_gender',
            header: 'Gender',
            sortable: true,
            filterable: true,
            filterType: FilterType.STRING,
            setting: {
                key: 'user_gender',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'Gender',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_email',
            header: 'E-mail',
            sortable: true,
            filterable: true,
            filterType: FilterType.STRING,
            setting: {
                key: 'user_email',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'E-mail',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_age',
            header: 'Age',
            sortable: true,
            filterable: true,
            filterType: FilterType.NUMBER,
            setting: {
                key: 'user_age',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'Age',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_country',
            header: 'Country',
            sortable: true,
            filterable: true,
            filterType: FilterType.STRING,
            setting: {
                key: 'user_country',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'Country',
                    disabled: false,
                },
            },
        },
        {
            field: 'user_number',
            header: 'Mobile',
            sortable: true,
            filterable: true,
            filterType: FilterType.NUMBER,
            setting: {
                key: 'user_number',
                component: TemplateFormComponentType.TEXTBOX,
                type: ControlType.TEXT,
                templateOptions: {
                    placeholder: 'Mobile',
                    disabled: false,
                },
            },
        },
    ],
    header: {
        show: true,
        title: 'Employee',
        globalFilter: true,
        buttons: [
            {
                type: ButtonConfigType.Single,
                options: {
                    type: ButtonType.button,
                    action: ActionButtonType.Add,
                    text: 'Add USER',
                    left: {
                        icon: 'flaticon2-plus',
                        show: true,
                    },
                    className: 'btn btn-brand btn-bold',
                },
            },

        ],
    },
    actions: {
        show: true,
        edit: true,
        view: true,
        delete: true,
        permission: {

        },
    },
    toggleOptions: {
        columns: [{ field: 'user_name', selected: true }, { field: 'user_last_name', selected: true }, { field: 'user_gender', selected: true }, { field: 'user_email', selected: true } ,   { field: 'user_age', selected: true } , { field: 'user_country', selected: true }, { field: 'user_number', selected: true }],
        show: true,
    },
    exportOptions: {
        show: true,
        csv: { show: true, type: ExportType.CUSTOM, action: ExportActionType.EXPORT_CSV },
        pdf: { show: true, type: ExportType.CUSTOM, action: ExportActionType.EXPORT_PDF },
        excel: { show: true, type: ExportType.CUSTOM, action: ExportActionType.EXPORT_EXCEL },


    },
};

