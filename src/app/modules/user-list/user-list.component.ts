import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActionButtonType, ButtonConfigType, ButtonType } from 'src/app/models/common/forms/button';
import { DropDownType } from 'src/app/models/common/forms/dropdown';
import { ControlType, Placement } from 'src/app/models/common/forms/form-field-config';
import { TemplateFormComponentType } from 'src/app/models/common/forms/template-driven';
import { ModalType } from 'src/app/models/common/modal/modal-config';
import { TableConfig } from 'src/app/models/common/table/table-config';
import { StateStoreService } from './../../state/state-store.service';
import { CREATE_CLIENT_AREA_TABLE_CONSTANT } from './user-list.constant';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  clientAreaList: Array<any> = [
    { user_name: 'kunjan', user_last_name: 'kunjan', user_age: '45', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'k.k.k', user_last_name: 'panchal', user_age: '50', user_gender: 'female', user_email: 'Kunj@123.com', user_country: 'Rajkot', user_number: '9662457812' },
    { user_name: 'Jigabhai', user_last_name: 'patel', user_age: '23', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'Jigabhai', user_last_name: 'shiroya', user_age: '78', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'Kunjan', user_last_name: 'admin', user_age: '75', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'j.j.j', user_last_name: 'kunjan', user_age: '100', user_gender: 'other', user_email: 'Kunj@123.com', user_country: 'Goa', user_number: '9662457812' },
    { user_name: 'kunjan', user_last_name: 'koladiya', user_age: '10', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'Mehul', user_last_name: 'panchal', user_age: '15', user_gender: 'female', user_email: 'Kunj@123.com', user_country: 'Ahemedabad', user_number: '9662457812' },
    { user_name: 'Jigabhai', user_last_name: 'kunjan', user_age: '23', user_gender: 'male', user_email: 'Kunj@123.com', user_country: 'Bombay', user_number: '9662457812' },

  ];
  count = 6;

  tableOptions = CREATE_CLIENT_AREA_TABLE_CONSTANT;


  optionsclient: TableConfig = {
    rows: 10,
    paginator: true,
    lazy: false,
    filterable: true,
    sortable: true,
    // sortMode: ModeType.SINGLE,
    reorderableColumns: true,
    resizableColumns: true,
    scrollable: true,
    scrollHeight: '200px',
    responsive: false,
    reorderableRows: false,
    dataKey: 'brand',
    customSort: false,
    //  selectionMode: ModeType.MULTIPLE,
    checkboxSelection: true,
    radioSelection: false,
    //  columnResizeMode: ResizeType.EXPAND,
    sticky: false,
    columns: [
      {
        field: 'brand',
        header: 'Brand',
        sortable: true,
        filterable: true,
        // filterType: FilterType.STRING,
        //   filterBy: FilterByType.CONTAIN,
        setting: {
          key: 'brand',
          component: TemplateFormComponentType.TEXTBOX,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'Brand',
            disabled: false
          }
        }
      },
      {
        field: 'lastYearSale',
        header: 'LastYearSale',
        sortable: true,
        filterable: true,
        // filterType: FilterType.SELECT,
        //  filterBy: FilterByType.IN,
        setting: {
          key: 'lastYearSale',
          component: TemplateFormComponentType.SELECT,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'Last Year Sale',
            disabled: false,
            selectOptions: {
              checkboxSelection: true,
              inputDropDown: true,
              selectAll: true,
              unSelectAll: true,
              closeOnSelect: false,
            },
            bindLabel: 'id',
            bindValue: 'name',
            multiple: true,
          }
        }
      },
      {
        field: 'thisYearSale',
        header: 'ThisYearSale',
        sortable: true, filterable: true,
        //  filterType: FilterType.SELECT,
        // filterBy: FilterByType.EQUAL,
        setting: {
          key: 'thisYearSale',
          component: TemplateFormComponentType.SELECT,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'This Year Sale',
            disabled: false,
            selectOptions: {
              closeOnSelect: true,
            },
            bindLabel: 'label',
            bindValue: 'value',
            multiple: false,
          }
        }
      },
      {
        field: 'lastYearProfit',
        header: 'LastYearProfit',
        sortable: true, filterable: true,
        //  filterType: FilterType.STRING,
        //  filterBy: FilterByType.CONTAIN,
        setting: {
          key: 'lastYearSale',
          component: TemplateFormComponentType.TEXTBOX,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'Last Year Sale',
            disabled: false
          }
        }
      },
      {
        field: 'color',
        header: 'Color',
        sortable: true,
        filterable: true,
        // filterType: FilterType.SELECT,
        //  filterBy: FilterByType.CONTAIN,
        setting: {
          key: 'color',
          component: TemplateFormComponentType.SELECT,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'This Year Sale',
            disabled: false,
            selectOptions: {
              closeOnSelect: true,
            },
            bindLabel: 'name',
            bindValue: 'id',
          }
        }, options: [{
          id: 'Green',
          name: 'Green'
        },
        {
          id: 'Red',
          name: 'Red'
        },
        {
          id: 'Yellow',
          name: 'Yellow'
        }]
      },
      {
        field: 'thisYearProfit',
        header: 'ThisYearProfit',
        sortable: true,
        filterable: true,
        //  filterType: FilterType.DATE,
        // filterBy: FilterByType.EQUAL,
        setting: {
          key: 'thisYearProfit',
          component: TemplateFormComponentType.DATEPICKER,
          type: ControlType.TEXT,
          templateOptions: {
            placeholder: 'ThisYearProfit',
            disabled: false
          }
        }
      },
    ],
    toggleOptions: {
      show: true,
      columns: [
        {
          field: 'thisYearProfit',
          selected: true,
        },
        {
          field: 'brand',
          selected: true,
        },
        {
          field: 'color',
          selected: true,
        }
      ]
    },
    header: {
      show: true,
      title: 'Add User',
      globalFilter: true,
      globalFilterFields: undefined,
      columnFilter: false,
      buttons: [
        {
          type: ButtonConfigType.Single,
          options: {
            type: ButtonType.button,
            text: 'Add New',
            left: {
              icon: 'flaticon2-plus',
              show: true,
            },
            className: 'btn btn-brand btn-sm'
          }
        }],
      dropdowns: [{
        classNames: {
          dropdownToggle: 'btn-brand btn-sm',
          dropdownMenu: 'dropdown-menu dropdown-menu-fit',
        },
        type: DropDownType.Default,
        placement: Placement.BottomRight,
        showFooter: false,
        showHeader: false,
        toggleOptions: {
          toggleText: 'Add Task Group'
        },
        options: [],
      }]
    },
    actions: {
      show: true,
      edit: true,
      delete: true,
    },
    actionButtonOptions: {
      show: true,
      title: 'Approve/Reject',
      buttons: [
        {
          type: ButtonConfigType.Single,
          options: {
            type: ButtonType.button,
            text: 'hfghfhg',
            action: ActionButtonType.Approve,
            className: 'btn btn-success btn-sm kt-mr-5',
            confirmation: {
              show: true,
              message: 'Are you want to sure approve this record?',
              type: ModalType.SUCCESS_CONFIRM,
              title: 'Approve'
            }
          },
        },
        {
          type: ButtonConfigType.Single,
          options: {
            type: ButtonType.button,
            action: ActionButtonType.Reject,
            text: 'hfghfhg',
            className: 'btn btn-danger btn-sm kt-mr-5',
            confirmation: {
              show: true,
              message: 'Are you want to sure reject this record?',
              type: ModalType.DELETE,
              title: 'Reject'
            }
          },
        },
      ],
    },
    stateKey: 'testing',
  };

  loaddata(payload) {
    if (payload.data.text === 'Add New') {
      this.router.navigate(['/user-list/add-user']);
    }
    else if (payload.data.text === 'Back') {
      this.router.navigate(['/user-list']);
    }

  }

  constructor(
    public router: Router,
    public stateStoreService: StateStoreService) { }


  ngOnInit(): void {
    ('REDUCERCALL');
    this.stateStoreService.dispatchJobCommon({
      callback: this.onTestCallback.bind(this), data: {},
      module: 'user',
      service: 'userService',
      method: 'getAllUser',
      state: 'getAllUser',
      successNotification: false
    });
  }

  onTestCallback(res) {
    (res);
  }

  clientAreaTableCallback(event) {
    ('fdsfds');
  }


}
