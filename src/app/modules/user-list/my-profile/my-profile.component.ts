import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss']
})
export class MyProfileComponent implements OnInit {


  myProfileForm = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl(''),
    age: new FormControl(''),
    mobile: new FormControl('')
  });


  constructor(public activeModal: NgbActiveModal, public router: Router) { }

  ngOnInit(): void {
    this.myProfileForm.patchValue({
      firstName : 'kunjan',
      lastName : 'Thakkar',
      email : 'kunj@123.COM',
      age : 10 ,
      mobile : 987635353673,
    });

    // this.myProfileForm.disable(); 
  }

  onSubmit() {
    this.router.navigate(['./user-list']);
    this.activeModal.close('Modal Closed');
  }

  closeClickHandler() {
    this.activeModal.close('Modal Closed');
  }

}
