import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserListRoutingModule } from './user-list-routing.module';
import { UserListComponent } from './user-list.component';
import { TableModule } from '../shared/components/table/table.module';
import { TableSubHeaderModule } from '../shared/components/table-sub-header/table-sub-header.module';
import { AddUserComponent } from './add-user/add-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BaseModalModule } from '../shared/components/modals/base-modal/base-modal.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { ToastrModule } from 'ngx-toastr'


@NgModule({
  declarations: [
    UserListComponent,
    AddUserComponent,
    ChangePasswordComponent,
    MyProfileComponent],
  imports: [
    CommonModule,
    UserListRoutingModule,
    TableModule,
    TableSubHeaderModule,
    ReactiveFormsModule,
    BaseModalModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    })
  ]
})
export class UserListModule { }
