import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {


  userForm: FormGroup;
  submitted = false

  constructor(public router: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      gender: [''],
      email: [''],
      age: [''],
      country: [''],
      mobile: ['']
    });
  }

  closeClickHandler() {
    this.router.navigate(['/user-list']);
  }

  onSubmit() {
    this.submitted = true;
    if (this.userForm.invalid) {
      Object.keys(this.userForm.controls).forEach(controlName =>
        this.userForm.controls[controlName].markAsTouched()
      )
    }
    else {
      this.router.navigate(['/user-list']);
    }
  }





}
