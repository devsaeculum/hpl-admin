import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr'


@Component({
  selector: 'kt-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm = new FormGroup({
    oldPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    confirmPassword: new FormControl('', [Validators.required, Validators.minLength(8)]),
  });

  constructor(public activeModal: NgbActiveModal, public router: Router, public toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.changePasswordForm.invalid) {
      Object.keys(this.changePasswordForm.controls).forEach(controlName =>
        this.changePasswordForm.controls[controlName].markAsTouched()
      );
      this.toastr.error('everything is broken', 'Major Error', {
        timeOut: 3000,
      });
    }
    else {
      this.router.navigate(['./user-list']);
      this.activeModal.close('Modal Closed');
      this.toastr.success('everything is broken', 'Major Error', {
        timeOut: 3000,
      });
    }



  }

  close() {
    this.activeModal.close('Modal Closed');
  }

}
