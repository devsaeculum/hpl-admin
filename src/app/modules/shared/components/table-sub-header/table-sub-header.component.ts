// ANGULAR
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

// EXTERNAL MODULES
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

// COMPONENTS
import { NotificationComponent } from '../modals/notification/notification.component';

// INTREFACES
import { ModalButtonType } from '../../../../models/common/modal/modal-config';
import { TableConfig, TableEventType } from '../../../../models/common/table/table-config';

// CONSTANTS
import { ModalBaseClass } from '../modals/base_class/modal-base-class';

@Component({
  selector: 'kt-table-sub-header',
  templateUrl: './table-sub-header.component.html',
  styleUrls: ['./table-sub-header.component.scss']
})
export class TableSubHeaderComponent extends ModalBaseClass implements OnInit {

  /**
   * External inputs
   */

  @Input() options: TableConfig

  @Input() count: any;

  /**
    * External outputs
    */
  @Output() subHeaderCallback = new EventEmitter<any>();

  constructor(public modalService: NgbModal) {
    super(modalService);
  }
  ngOnInit() {
  }

 


  /**
   * Emit event on state restored
   */
  buttonClick(data, rowData = null) {
    (data);

    if (data.confirmation && data.confirmation.show) {
      this.confirmationCallback(data.confirmation, () => {
        this.subHeaderCallback.emit({ type: TableEventType.ACTION, data: { ...data, ...rowData } });
      });
      return;
    }
    this.subHeaderCallback.emit({ type: TableEventType.ACTION, data: { ...data, ...rowData } })
  }

  /**
  * Emit event on state restored
  */
  dropdownClick(data) {
    if (data.confirmation && data.confirmation.show) {
      this.confirmationCallback(data.confirmation, () => {
        this.subHeaderCallback.emit({ type: TableEventType.ACTION, data: { ...data } });
      });
      return;
    }
    this.subHeaderCallback.emit({ type: TableEventType.ACTION, data: { ...data } })
  }

  /**
  * Confirmation callback
  */
  confirmationCallback(modal, callback = null) {
    const modalRef = this.openPopUp(NotificationComponent, { options: modal });
    modalRef.result.then(result => {
      if (result.STATUS === ModalButtonType.DELETE || result.STATUS === ModalButtonType.OK) {
        callback && callback();
      }
    });
  }



}
