// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// EXTERNAL DEPENDENCY
import { TranslateModule } from '@ngx-translate/core';
import { NgbTooltipModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';

// MODULES
import { ButtonModule } from '../buttons/button/button.module';
import { NotificationModule } from '../modals/notification/notification.module';
import { DropDownModule } from '../dropdowns/dropdown/dropdown.module';

// COMPONENTS
import { TableSubHeaderComponent } from './table-sub-header.component';

@NgModule({
  declarations: [TableSubHeaderComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************EXTERNAL MODULES*******************/
    TranslateModule.forChild(),
    NgbDropdownModule,
    NgbTooltipModule,
    NgxPermissionsModule.forChild(),
    /********************CUSTOM MODULES*******************/
    ButtonModule,
    NotificationModule,
    DropDownModule,
  ], exports: [TableSubHeaderComponent]
})
export class TableSubHeaderModule { }
