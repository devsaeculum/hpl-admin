// ANGULAR DEPENDANCY
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';

// INTERFACES AND ENUMS
import { ButtonConfig, ButtonConfigType, ButtonOption } from '../../../../../models/common/forms/button';

/**
 * This component is used for rendering single or group button
 *
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 03/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>)  button component created
 */
@Component({
  selector: 'kt-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ButtonComponent {
  /**
   * External inputs
   */
  @Input() buttonConfig:any;
  @Input() loading = false;
  @Input() permission: string = null;

  /**
   * External outputs
   */
  @Output() buttonClick = new EventEmitter<any>();

  /**
   * Button type config constants
   */
  buttonConfigType = ButtonConfigType;

  /**
   *
   * Emits event on button click with associated data
   * @param {ButtonOption} option
   */
  clickHandler(option: ButtonOption) {
    this.buttonClick.emit(option);
  }
}
