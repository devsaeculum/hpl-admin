// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbModule, NgbTooltipModule  } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { NgxPermissionsModule } from 'ngx-permissions';

// COMPONENTS
import { ButtonComponent } from './button.component';

/**
 * This component used for generation of different types of buttons
 *
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 03/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>) button component created
 */

@NgModule({
  declarations: [ButtonComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************External MODULES*******************/
    NgbModule,
    NgbTooltipModule,
    TranslateModule.forChild(),
    NgxPermissionsModule.forChild(),
  ],
  exports: [
    ButtonComponent,
  ],
})
export class ButtonModule { }
