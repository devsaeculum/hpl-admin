// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// COMPONENTS
import { LoaderComponent } from './loader.component';

/**
 * This component used for generation of different types of buttons
 *
 * @author Ruchita vani <ruchita.vani@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 07/04/2020 (Ruchita vani <ruchita.vani@saeculumsolutions.com>) loader component created
 */

@NgModule({
  declarations: [LoaderComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************External MODULES*******************/
    NgbModule,

  ],
  exports: [
    LoaderComponent,
  ],
})
export class LoaderModule { }
