// ANGULAR DEPENDANCY
import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Input, ViewChild } from '@angular/core';

/**
 * This component is used for full page loader or sectional loader
 *
 * @author Ruchita vani <ruchita.vani@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 07/04/2020 (ruchita vani  <ruchita.vani@saeculumsolutions.com>)  loader component created
 */

@Component({
  selector: 'kt-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderComponent implements AfterViewInit {
  /**
   * External inputs
   */
  _showLoader: boolean;
  @Input('showLoader')
  set showLoader(value: boolean) {
    this._showLoader = value;
    if (this._showLoader) {
      this.setHeightWidth();
    }
  }

  get showLoader(): boolean {
    return this._showLoader;
  }
  /**
   *
   * Loader height calculated each time showloader property changes base on the content
   */
  loaderHeight = 'auto';
  /**
  *
  * Loader  Width calculated each time showloader property changes base on the content
  */
  loaderWidth = 'auto';

  /**
   *
   * Reference to the content on which the loader needs to be shown
   */
  @ViewChild('content', { static: false }) contentRef: ElementRef;

  constructor(private cdr: ChangeDetectorRef) {
  }

  /**
   *
   * Sets the height and width of the loader based on the content's height and widht
   */
  setHeightWidth() {
    if (this.contentRef) {
      setTimeout(() => {
        const boundingClientReact = this.contentRef.nativeElement.getBoundingClientRect();
        // set height and width
        this.loaderHeight = `${boundingClientReact.height > window.innerHeight ? window.innerHeight : boundingClientReact.height}px`;
        this.loaderWidth = `${boundingClientReact.width > window.innerWidth ? window.innerWidth : boundingClientReact.width}px`;

        if (!this.cdr['destroyed']) {
          this.cdr.detectChanges();
        }
      }, 200)
    }
  }

  /**
   *
   * Lifecycle hook called after the view gets initialized
   */
  ngAfterViewInit() {
    this.setHeightWidth();
  }

}
