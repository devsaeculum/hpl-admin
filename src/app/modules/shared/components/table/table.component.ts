// ANGULAR
import { Component, Input, Output, OnInit, EventEmitter, ViewChild, ViewEncapsulation, SimpleChanges, AfterViewInit, ChangeDetectorRef } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { uniqBy, cloneDeep } from 'lodash';

// Components
import { Table } from '../../custom/prime-table/table';
import { NotificationComponent } from '../modals/notification/notification.component';


// PIPES
// import { TimezoneDateTime } from '../../../../../../app/core/pipes/date-pipes/timezone-datetime.pipe';
// import { TimezoneDate } from '../../../../../../app/core/pipes/date-pipes/timezone-date.pipe';

// INTERFACE AND ENUMS
import { ModalButtonType } from '../../../../models/common/modal/modal-config';
import { TemplateFormComponentType } from '../../../../models/common/forms/template-driven';

// CONSTANTS
import { TableConfig, TableEventType, ExportType, ExportOption, ExportActionType, FilterType } from '../../../../models/common/table/table-config';
import { DEFAULT_OPTIONS, DELETE_MODAL_CONSTANTS, NO_DATA_MODAL_CONSTANTS } from './table.constant';


// BASE CLASS
import { ModalBaseClass } from '../modals/base_class/modal-base-class';
import { DropDownOption } from '../../../../models/common/forms/dropdown';
import { TranslateService } from '@ngx-translate/core';
// import moment from 'moment-timezone';


import * as moment from 'moment'

/**
 * This component used for render table for with dynamic columns and rows
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) table component created
 */
@Component({
  selector: 'kt-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  styles: [`
        :host ::ng-deep .ui-table .iq-sticky-table .ui-table-thead > tr > th {
            position: -webkit-sticky;
            position: sticky;
            top: 69px;
            box-shadow: 1px 3px 6px 0 rgba(32,33,36,0.10);
        }

        @media screen and (max-width: 64em) {
            :host ::ng-deep .ui-table .iq-sticky-table .ui-table-thead > tr > th {
                top: 99px;
            }
        }
`],
  // providers: [TimezoneDateTime, TimezoneDate],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent extends ModalBaseClass implements OnInit, AfterViewInit {
  /**
    * External inputs
    */


  displayType: any;

  _options: TableConfig;
  get options(): TableConfig {
    return this._options;
  }

  @Input('options')
  set options(value: TableConfig) {
    if (!value) return;

    this.optionsClone = cloneDeep(value);
    this.handleOptions(value);
  }

  _filterOptions: any;
  get filterOptions(): any {
    return this._options;
  }

  @Input('filterOptions')
  set filterOptions(value: any) {
    if (!value) return;
    this._filterOptions = value;
    this.fillOptions();
  }

  @Input() rows: any;

  @Input() frozenRows: any;

  @Input() count: any;

  @Input() loading: boolean = false;

  /**
   * External templates inputs
   */
  @Input() headerTemplate: any;

  @Input() itemTemplate: any;

  @Input() bodyTemplate: any;

  @Input() summaryTemplate: any;

  @Input() frozenRowsTemplate: any;

  @Input() footerTemplate: any;

  @Input() rowExpansionTemplate: any;

  /**
    * External outputs
    */
  @Output() tableCallback = new EventEmitter<any>();

  /**
    * Other variables
    */
  first = 0;
  selection = [];
  allColumns = [];
  allFrozenColumns = [];
  componentType = TemplateFormComponentType;
  filterItems = {};
  toggleColumns = [];
  optionsClone;
  resetDisable = true;
  filters = {};
  globalFilter = '';
  filterType = FilterType;
  // displayType = DisplayType;

  @ViewChild('table', { static: false }) table: Table;
  constructor(public modalService: NgbModal, public translateService: TranslateService) {
    super(modalService);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.fillOptions();
    this.restoreOptions();
  }

  handleOptions(value) {
    // translate column names
    value.columns && value.columns.forEach(column => {
      column.header = this.translateService.instant(column.header);
    });

    // translate frozen column names
    value.frozenColumns && value.frozenColumns.forEach(column => {
      column.header = this.translateService.instant(column.header);
    });

    // translate frozen column names
    value.toggleOptions && value.toggleOptions.columns && value.toggleOptions.columns.forEach(column => {
      const match = value.columns.find(a => a.field === column.field);
      column.header = match ? match.header : column.field;
    });

    // merge all options
    this._options = {
      ...DEFAULT_OPTIONS,
      ...value,
      actions: {
        ...DEFAULT_OPTIONS.actions,
        ...value.actions
      }
    };
    this.allColumns = cloneDeep(this._options.columns || []);
    this.allFrozenColumns = this._options.frozenColumns || [];
    this.columnToggle();
  }

  restoreOptions() {
    if (this._options.toggleOptions && this._options.toggleOptions.columns) {
      const storage = this.table.getStorage();
      if (storage) {
        const stateString = null; //storage.getItem(this._options.stateKey);

        if (stateString) {
          let state = JSON.parse(stateString);
          this._options.toggleOptions.columns.forEach(column => {
            column.selected = state.toggleColumns ? !!state.toggleColumns.find(a => a === column.field) : true;
          });

          Object.keys(state.filters || {}).forEach(filter => {
            if (filter === 'global') {
              this.globalFilter = state.filters[filter].value;
            } else {
              this.filters[filter] = state.filters[filter].value;
            }
          })
        }
        this.columnToggle();
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filterOptions && changes.filterOptions.currentValue) {
      this.fillOptions();
    } else if (changes.rows && changes.rows.currentValue) {
      this.fillOptions();
    }
  }

  /**
  * Toggle Columns on change on columns toggle
  */
  columnToggle() {
    setTimeout(() => {
      if (this._options.toggleOptions && this._options.toggleOptions.columns) {
        this._options.columns = this.allColumns.filter(a =>
          !this._options.toggleOptions.columns.find(b => b.field === a.field) ||
          this._options.toggleOptions.columns.find(b => b.field === a.field && b.selected)
        );
        this._options.frozenColumns = this.allFrozenColumns.filter(a =>
          !this._options.toggleOptions.columns.find(b => b.field === a.field) ||
          this._options.toggleOptions.columns.find(b => b.field === a.field && b.selected)
        );

        this.toggleColumns = this._options.toggleOptions.columns.filter(a => a.selected).map(a => a.field);
        // save in table state
        setTimeout(() => {
          if (this.table && this.table.isStateful()) {
            this.table.saveState();
          }
        }, 500);
      }
    }, 100);
  }

  collapseExpandFilters() {
    this._options.expandFilterEnable = !this._options.expandFilterEnable;
    if (!this._options.expandFilterEnable) {
      Object.keys(this.filters || {}).forEach(key => {
        this.filters[key] = null;
      });
      this.globalFilter = '';
      this.table.clearFilters();
    }
  }

  /**
  * Fill options for select & multiselect filter type
  */
  fillOptions() {
    if (!this.rows || !this._options || !this._options.columns) return;
    this._options.columns.forEach(a => {
      if (a.filterable && (a.filterType === FilterType.SELECT || a.filterType === FilterType.MULTI_SELECT)) {
        if (this._filterOptions && this._filterOptions[a.field]) {
          this.filterItems[a.field] = this._filterOptions[a.field]
        } else if (a.options) {
          this.filterItems[a.field] = of(a.options);
        } else {
          this.filterItems[a.field] = of(uniqBy(this.rows.map(b => b[a.field] && ({ value: b[a.field], label: b[a.field] })).filter(c => c), 'value'));
        }
      }
    });
  }

  onFilterChange(event) {
    this.filters[event.key] = event.value;
    const columnToBeSorted = this._options.columns.find(column => column.field === event.key);
    if (this._options.lazy) {
      this.table.filter(event.value, (columnToBeSorted.field), columnToBeSorted.filterType);
    } else {
      this.table.filter(event.value, columnToBeSorted.field, columnToBeSorted.filterBy)
    }
  }

  onReset() {
    // reset all filters
    Object.keys(this.filters || {}).forEach(key => {
      this.filters[key] = null;
    });
    this.globalFilter = '';
    this.table.clearState();
    this.handleOptions(cloneDeep(this.optionsClone));
    this.resetDisable = false;
    setTimeout(() => {
      this.resetDisable = true;
    }, 100);
  }

  /**
   * Emit event on row select with selected row
   */
  onRowSelect(data) {
    this.tableCallback.emit({ type: TableEventType.SELECT, data: data })
  }

  /**
  * Emit event on row unselect with selected row
  */
  onRowUnselect(data) {
    this.tableCallback.emit({ type: TableEventType.UN_SELECT, data: data })
  }

  /**
  * Emit event on page change
  */
  onPage(data) {
    this.tableCallback.emit({ type: TableEventType.PAGE, data: data })
  }

  /**
  * Emit event on sorting
  */
  onSort(data) {
    this.tableCallback.emit({ type: TableEventType.SORT, data: data })
  }

  /**
   * Emit event on global filter and inline filter
   */
  onFilter(data) {
    this.tableCallback.emit({ type: TableEventType.FILTER, data: data })
  }

  /**
   * Emit event on server side config change like paging, sort, filter, page size change
   */
  onLazyLoad(data) {
    this.tableCallback.emit({ type: TableEventType.LAZY, data: data })
  }

  /**
   * Emit event on row expanded
   */
  onRowExpand(data) {
    this.tableCallback.emit({ type: TableEventType.EXPAND, data: data })
  }

  /**
   * Emit event on row collapsed
   */
  onRowCollapse(data) {
    this.tableCallback.emit({ type: TableEventType.COLLAPSE, data: data })
  }

  /**
   * Emit event on context menu open
   */
  onContextMenuSelect(data) {
    this.tableCallback.emit({ type: TableEventType.CONTEXT_MENU, data: data })
  }

  /**
    * Emit event on column resize change
    */
  onColResize(data) {
    this.tableCallback.emit({ type: TableEventType.RESIZE, data: data })
  }

  /**
    * Emit event on column reorder change
    */
  onColReorder(data) {
    this.tableCallback.emit({ type: TableEventType.COL_REORDER, data: data })
  }

  /**
   * Emit event on row reorder change
   */
  onRowReorder(data) {
    this.tableCallback.emit({ type: TableEventType.ROW_REORDER, data: data })
  }

  /**
   * Emit event on header cpheckbox toggle
   */
  onHeaderCheckboxToggle(data) {
    this.tableCallback.emit({ type: TableEventType.TOGGLE, data: data })
  }

  /**
   * Emit event on first page no change
   */
  firstChange(data) {
    this.tableCallback.emit({ type: TableEventType.FIRST, data: data })
  }

  /**
   * Emit event on page row size change
   */
  rowsChange(data) {
    this.tableCallback.emit({ type: TableEventType.ROW, data: data })
  }

  /**
   * Emit event on state save for storage
   */
  onStateSave(data) {
    this.tableCallback.emit({ type: TableEventType.STATE_SAVE, data: data })
  }

  /**
   * Emit event on state restored
   */
  onStateRestore(data) {
    this.tableCallback.emit({ type: TableEventType.STATE_RESTORE, data: data })
  }

  /**
   * Emit event on active/inactive 
   */
  enableDisableClick(data) {
    this.tableCallback.emit(data);
  }

  /**
   * Emit event on state restored
   */
  buttonClick(data, rowData = null) {
    if (data.confirmation && data.confirmation.show) {
      this.confirmationCallback(data.confirmation, () => {
        this.tableCallback.emit({ type: TableEventType.ACTION, data: { ...data, ...rowData, selection: this.selection } });
      });
      return;
    }
    this.tableCallback.emit({ type: TableEventType.ACTION, data: { ...data, ...rowData, selection: this.selection } })
  }

  /**
  * Emit event on state restored
  */
  dropdownClick(data) {
    if (data.confirmation && data.confirmation.show) {
      this.confirmationCallback(data.confirmation, () => {
        this.tableCallback.emit({ type: TableEventType.ACTION, data: { ...data } });
      });
      return;
    }
    this.tableCallback.emit({ type: TableEventType.ACTION, data: { ...data } })
  }

  /**
   * Emit event on state restored
   */
  editClick(data) {
    this.tableCallback.emit({ type: TableEventType.EDIT, data: data })
  }

  /**
   * Emit event on state restored
   */
  viewClick(data) {
    this.tableCallback.emit({ type: TableEventType.VIEW, data: data })
  }

  /**
   * Emit event on state restored
   */
  deleteClick(data) {
    if (this._options.actions && this._options.actions.deleteConfirmation) {
      this.confirmationCallback(DELETE_MODAL_CONSTANTS, () => {
        this.tableCallback.emit({ type: TableEventType.DELETE, data: data });
      });
      return;
    }

    this.tableCallback.emit({ type: TableEventType.DELETE, data: data })
  }

  /**
   * Confirmation callback
   */
  confirmationCallback(modal, callback = null) {
    const modalRef = this.openPopUp(NotificationComponent, { options: modal });
    modalRef.result.then(result => {
      if (result.STATUS === ModalButtonType.DELETE || result.STATUS === ModalButtonType.OK) {
        callback && callback();
      }
    });
  }

  /**
   * Emit event and customize sort function
   */
  sortFunction(event) {
    event.data.sort((data1, data2) => {
      let value1 = data1[event.field];
      let value2 = data2[event.field];
      let result = null;

      if (value1 == null && value2 != null)
        result = -1;
      else if (value1 != null && value2 == null)
        result = 1;
      else if (value1 == null && value2 == null)
        result = 0;
      else if (typeof value1 === 'string' && typeof value2 === 'string')
        result = value1.localeCompare(value2);
      else
        result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;

      return (event.order * result);
    });
    this.tableCallback.emit({ type: TableEventType.SORT, data: event });
  }

  /**
   * Emit event for bulk option
   */
  bulkOptionClick(event: DropDownOption) {
    if (event.confirmation && event.confirmation.show) {
      this.confirmationCallback(event.confirmation, () => {
        this.tableCallback.emit({ type: TableEventType.ACTION, data: event, selected: this.selection })
      });
      return;
    }
    this.tableCallback.emit({ type: TableEventType.ACTION, data: event, selected: this.selection })
  }

  /**
   * Emit event for export option
   */
  exportClick(event: ExportOption) {
    // open alert when data available
    if (!this.rows || !this.rows.length) {
      this.confirmationCallback(NO_DATA_MODAL_CONSTANTS);
      return;
    }

    if (event.type === ExportType.INBUILT) {
      switch (event.action) {
        case ExportActionType.EXPORT_PDF:
          this.exportPdf(this.rows);
          break;
        case ExportActionType.EXPORT_EXCEL:
          this.exportExcel(this.rows);
          break;
        case ExportActionType.EXPORT_CSV:
          this.exportCsvFile(this.rows);
          break;
        default:
          break;
      }
      return;
    }
    this.tableCallback.emit({ type: TableEventType.EXPORT, data: { event, columns: this._options.columns, rows: this.rows, options: this._options } });
    return;
  }

  /**
    * Export pdf for inbuilt feature and custom export
    */
  exportPdf(rows) {
    // import("jspdf").then(jsPDF => {
    //   import("jspdf-autotable").then(x => {
    //     const doc = new jsPDF.default(0, 0);
    //     doc.autoTable(this._options.columns.map(col => ({ title: col.header, dataKey: col.field })), this.makePdfExportData(rows));
    //     let fileName = this._options.exportFileName;
    //     if (this._options && this._options.header && this._options.header.title) {
    //       fileName = this.translateService.instant(this._options.header.title);
    //     }
    //     doc.save(`${fileName}_${moment().milliseconds()}_IQCheckPoint.pdf`);
    //   });
    // });
  }

  /**
    * Export excel for inbuilt feature and custom export
    */
  exportExcel(rows) {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.makeExcelExportData(rows));
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      let fileName = this._options.exportFileName;
      if (this._options && this._options.header && this._options.header.title) {
        fileName = this.translateService.instant(this._options.header.title);
      }
      this.saveAsExcelFile(excelBuffer, fileName);
    });
  }

  makeExcelExportData(rows) {
    return rows.map(row => {
      let data = {};
      // this._options.columns.map(column => {
      //   if (column.displayType === DisplayType.DATE) {
      //     data[column.header] = this.timezoneDatePipe.transform(row[column.field]);
      //   } else if (column.displayType === DisplayType.DATETIME) {
      //     data[column.header] = this.timezoneDateTimePipe.transform(row[column.field]);
      //   } else {
      //     data[column.header] = row[column.field];
      //   }
      // })
      return data;
    });
  }

  makePdfExportData(rows) {
    return rows.map(row => {
      let data = {};
      // this._options.columns.map(column => {
      //   if (column.displayType === DisplayType.DATE) {
      //     data[column.field] = this.timezoneDatePipe.transform(row[column.field]);
      //   } else if (column.displayType === DisplayType.DATETIME) {
      //     data[column.field] = this.timezoneDateTimePipe.transform(row[column.field]);
      //   } else {
      //     data[column.field] = row[column.field];
      //   }
      // })
      return data;
    });
  }

  /**
    * Download excel using file-saver
    */
  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, `${fileName}_${moment().milliseconds()}_IQCheckPoint${EXCEL_EXTENSION}`);
    });
  }

  /**
   * Export csv for inbuilt feature and custom export
   */
  exportCsv(options = null) {
    let fileName = this._options.exportFileName;
    if (this._options && this._options.header && this._options.header.title) {
      fileName = this.translateService.instant(this._options.header.title);
    }
    this.table.exportCSV(options, `${fileName}_${moment().milliseconds()}_IQCheckPoint`);
  }

  /**
  * Export csv for external feature and custom export
  */
  exportCsvFile(rows) {
    let fileName = this._options.exportFileName;
    if (this._options && this._options.header && this._options.header.title) {
      fileName = this.translateService.instant(this._options.header.title);
    }
    this.table.exportCSVFile(this.makePdfExportData(rows), `${fileName}_${moment().milliseconds()}_IQCheckPoint`);
  }
}
