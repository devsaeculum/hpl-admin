
// import { SaSwitchModule } from '../../template-driven-components/components/sa-switch/sa-switch.module';

// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// EXTERNAL DEPENDENCY
import { TranslateModule } from '@ngx-translate/core';
import { NgbTooltipModule, NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPermissionsModule } from 'ngx-permissions';

// MODULES
import { TableModule as PrimeTableModule } from '../../custom/prime-table/table';
import { NotificationModule } from '../modals/notification/notification.module';
import { ButtonModule } from '../buttons/button/button.module';
import { SaTextboxModule } from '../template-drivem-components/component/sa-textbox/sa-textbox.module';
import { SaSelectModule } from '../template-drivem-components/component/sa-select/sa-select.module';
import { SaDatepickerModule } from '../template-drivem-components/component/sa-datepicker/sa-datepicker.module';
import { DropDownModule } from '../dropdowns/dropdown/dropdown.module';
import { LoaderModule } from '../loader/loader/loader.module';


// COMPONENTS
import { TableComponent } from './table.component';

// PIPES
import { DatePipeModule } from '../../../../../app/core/pipes/date-pipes/date.pipe.module';
import { LayoutModule } from 'src/app/modules/layout/layout.module';





/**
 * This component used for render tables
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 03/04/2020 ( Pratik Shihora <pratik@saeculumsolutions.com>) table module created
 */
@NgModule({
  declarations: [TableComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    FormsModule,
    /********************EXTERNAL MODULES*******************/
    TranslateModule.forChild(),
    PrimeTableModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgxPermissionsModule.forChild(),
    /********************CUSTOM MODULES*******************/
    NotificationModule,
    LoaderModule,
    ButtonModule,
    DropDownModule,
    SaTextboxModule,
    SaSelectModule,
    SaDatepickerModule,
    /********************PIPES*******************/
    DatePipeModule,
    LayoutModule
  ],
  exports: [
    TableComponent,
  ],
})
export class TableModule { }