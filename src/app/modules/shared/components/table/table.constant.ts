// INTERFACE ANF ENUMS
import { TableConfig, ResizeType, ExportType, ModeType, StorageType, ExportActionType, BulkActionType } from '../../../../models/common/table/table-config';
import { ModalConfig, ModalType } from '../../../../models/common/modal/modal-config';
import { ButtonConfigType, ButtonType, ActionButtonType } from '../../../../models/common/forms/button';

// CONSTANTS
import { DELETE_ALL_BUTTON_CONSTANT, UPDATE_STATUS_BUTTON_CONSTANT } from '../../../../constants/translate-message/button';

export const DEFAULT_OPTIONS: TableConfig = {
  dataKey: 'id',
  //   columnFilter: false,
  columnResizeMode: ResizeType.NONE,
  sortable: true,
  filterable: false,
  expandFilterEnable: false,
  customSort: false,
  expandable: false,
  exportEnable: false,
  exportOptions: {
    show: true,
    csv: {
      show: true,
      type: ExportType.INBUILT,
      action: ExportActionType.EXPORT_CSV
    },
    excel: {
      show: true,
      type: ExportType.INBUILT,
      action: ExportActionType.EXPORT_EXCEL
    },
    pdf: {
      show: true,
      type: ExportType.INBUILT,
      action: ExportActionType.EXPORT_PDF
    }
  },
  frozenColumns: [],
  frozenWidth: undefined,
  toggleOptions: {
    show: false,
  },
  lazy: false,
  metaKeySelection: false,
  paginator: true,
  reorderableColumns: true,
  reorderableRows: false,
  resizableColumns: true,
  responsive: false,
  rows: 20,
  rowsPerPageOptions: [5, 10, 15, 20, 50, 100, 500],
  scrollHeight: '300px',
  scrollable: false,
  columns: [],
  sortMode: ModeType.SINGLE,
  stateKey: 'local-test',
  stateStorage: StorageType.LOCAL,
  style: '',
  sticky: false,
  radioSelection: false,
  checkboxSelection: false,
  footerVisible: false,
  exportFileName: 'iq',
  header: undefined,
  actions: {
    show: false,
    deleteConfirmation: true,
    edit: false,

  },
  actionButtonOptions: undefined,
  bulkOptions: {
    show: false,
    buttons: [
      {
        type: ButtonConfigType.Single,
        options: {
          type: ButtonType.button,
          text: DELETE_ALL_BUTTON_CONSTANT.TEXT,
          action: ActionButtonType.Delete,
          confirmation: {
            message: 'Are you sure you want to delete all records?',
            show: true,
            title: 'Delete All',
            type: ModalType.DELETE
          },
          className: 'btn btn-outline-danger btn-sm',
        },
      },
      {
        type: ButtonConfigType.Single,
        options: {
          type: ButtonType.button,
          action: ActionButtonType.Add,
          text: UPDATE_STATUS_BUTTON_CONSTANT.TEXT,
          className: 'btn btn-outline-success btn-sm',
        },
      },
    ],
    options: [{
      text: 'Delete Selected',
      seperator: false,
      action: BulkActionType.DELETE_SELECTED
    },
    {
      text: 'Update Status',
      seperator: false,
      action: BulkActionType.UPDATE_STATUS
    }]
  }
}

export const DELETE_MODAL_CONSTANTS: ModalConfig = {
  type: ModalType.DELETE,
  title: 'Delete',
  message: 'Are you sure you want to delete this record?',
};

export const NO_DATA_MODAL_CONSTANTS: ModalConfig = {
  type: ModalType.ALERT,
  title: 'Alert',
  message: 'No data available for export.',
};
