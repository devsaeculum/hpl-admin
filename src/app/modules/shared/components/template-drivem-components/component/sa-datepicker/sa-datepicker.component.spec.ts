import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaDatepickerComponent } from './sa-datepicker.component';

describe('SaDatepickerComponent', () => {
  let component: SaDatepickerComponent;
  let fixture: ComponentFixture<SaDatepickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaDatepickerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaDatepickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
