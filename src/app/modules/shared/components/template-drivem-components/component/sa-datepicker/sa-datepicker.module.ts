// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// EXTERNAL DEPENDENCY
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';

// COMPONENTS
import { SaDatepickerComponent } from './sa-datepicker.component';

/**
 * This component used for datepicker template driven form implementation
 * for datepicker type: datepicker
 *
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 03/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>) datepicker form component created
 */
@NgModule({
  declarations: [SaDatepickerComponent],
  imports: [
    CommonModule,
    FormsModule,
    /********************External MODULES*******************/
    NgbDatepickerModule,
    TranslateModule.forChild(),
    /********************CUSTOM MODULES*******************/
  ],
  exports: [SaDatepickerComponent]
})
export class SaDatepickerModule { }
