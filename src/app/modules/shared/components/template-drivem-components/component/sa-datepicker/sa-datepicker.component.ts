// ANGULAR
import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
//import moment from 'moment';
import * as moment from 'moment'

// UTILS
import { DateUtil, NgbDateFRParserFormatter } from '../../../../../../core/utils/date-util';

/**
 * This component used for date template driven form implementation
 * for input type: date
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) date template driven form component created
 */
@Component({
  selector: 'kt-sa-datepicker',
  templateUrl: './sa-datepicker.component.html',
  styleUrls: ['./sa-datepicker.component.scss'],
  providers: [{ provide: NgbDateParserFormatter, useClass: NgbDateFRParserFormatter }],
})
export class SaDatepickerComponent implements OnInit, OnChanges {
  /**
   * External inputs
   */
  @Input() value;
  @Input() setting;
  @Input() options;

  /**
   * External outputs
   */
  @Output() valueChange = new EventEmitter<any>();

  minDate: any = null;
  maxDate: any = null;

  // today date
  today = { month: (moment().format('MM') + 1), year: moment().format('YYYY'), day: moment().format('DD') };

  
  ngOnInit() {
    this.setConfigValidations();
  }

  /**
   * Set Validations for min and max dates
   */
  setConfigValidations() {
    if (this.setting && this.setting.validations && this.setting.validations.validators) {
      // set min date
      if (this.setting.validations.validators.min_date) {
        this.minDate = DateUtil.parse(this.setting.validations.validators.min_date);
      }
      // set max date
      if (this.setting.validations.validators.max_date) {
        this.maxDate = DateUtil.parse(this.setting.validations.validators.max_date);
      }
    }
  }

  /**
  * Get called when input property changes
  */
  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.setting && changes.setting.currentValue) {
      this.setConfigValidations();
    }
  }

  /**
	 * Emits the changed value when value changes
	 */
  notifyChange() {
    this.valueChange.emit({ key: this.setting.key, value: this.value });
  }

}
