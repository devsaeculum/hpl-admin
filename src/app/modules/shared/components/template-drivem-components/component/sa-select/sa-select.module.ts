// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// EXTERNAL DEPENDENCY
import { TranslateModule } from '@ngx-translate/core';
import { NgSelectModule } from '@ng-select/ng-select';

// COMPONENTS
import { SaSelectComponent } from './sa-select.component';

/**
 * This module is for configure all Select Template Driven Form Component related stuff.
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>)
 */
@NgModule({
  declarations: [SaSelectComponent],
  imports: [
    CommonModule,
    FormsModule,
    /********************External MODULES*******************/
    TranslateModule.forChild(),
    NgSelectModule,
  ],
  exports: [
    SaSelectComponent
  ]
})
export class SaSelectModule { }
