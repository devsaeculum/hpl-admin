// ANGULAR
import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';

// RX JS
import { of } from 'rxjs';

/**
 * This component used for select template driven form implementation
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) select template driven form component created
 */
@Component({
  selector: 'kt-sa-select',
  templateUrl: './sa-select.component.html',
  styleUrls: ['./sa-select.component.scss']
})
export class SaSelectComponent implements OnInit {
  /**
   * External inputs
   */
  @Input() value;
  @Input() setting;
  @Input() options;

  /**
   * External outputs
   */
  @Output() valueChange = new EventEmitter<any>();

  defaultOptions = of([]);
  filterText = '';
  items = [];

  ngOnInit() {
    this.setItems();
  }

  /**
	 * Emits the changed value when value changes
	 * @param {{ target: { value: any; }; }} event (contains changed value)
	 */
  notifyChange(event: { target: { value: any; }; }) {
    this.valueChange.emit({ key: this.setting.key, value: this.value });
  }

  /**
    * To get the option either by calling an api of from forntend
    */
  setItems() {
    if (this.options) {
      this.options.subscribe(values => this.items = values);
    }
  }

  /**
   * To select all the options
   */
  selectAll() {
    (this.options);
    const key = this.setting.templateOptions.bindValue;
    this.value = key ? this.items.map(item => item[key]) : this.options;
  }

  /**
   * To unselect all the options
   */
  unSelectAll() {
    this.value = this.setting.templateOptions.multiple ? [] : null;
  }
}
