// ANGULAR
import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * This component used for input template driven form implementation
 * for input type: password, text, number, email, url
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) textbox template driven form component created
 */
@Component({
  selector: 'kt-sa-textbox',
  templateUrl: './sa-textbox.component.html',
  styleUrls: ['./sa-textbox.component.scss']
})
export class SaTextboxComponent {

  /**
   * External inputs
   */
  @Input() value;
  @Input() setting;
  @Input() options;

  /**
   * External outputs
   */
  @Output() valueChange = new EventEmitter<any>();

  /**
	 * Emits the changed value when value changes
	 * @param {{ target: { value: any; }; }} event (contains changed value)
	 */
  // notifyChange(event: { target: { value: any; }; }) {
  //   this.valueChange.emit({ key: this.setting.key, value: event.target.value });
  // }

  notifyChange(event:any) {
    this.valueChange.emit({ key: this.setting.key, value: event.target.value });
  }

}
