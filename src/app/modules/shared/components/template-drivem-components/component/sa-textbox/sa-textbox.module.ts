// ANGULAR
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

// EXTERNAL DEPENDENCY
import { TranslateModule } from '@ngx-translate/core';

// COMPONENTS
import { SaTextboxComponent } from './sa-textbox.component';

/**
 * This module is for configure all Textbox Template Driven Form Component related stuff.
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>)
 */
@NgModule({
  declarations: [SaTextboxComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    FormsModule,
    /********************External MODULES*******************/
    TranslateModule.forChild()
  ],
  exports: [SaTextboxComponent]
})
export class SaTextboxModule { }
