import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaTextboxComponent } from './sa-textbox.component';

describe('SaTextboxComponent', () => {
  let component: SaTextboxComponent;
  let fixture: ComponentFixture<SaTextboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaTextboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaTextboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
