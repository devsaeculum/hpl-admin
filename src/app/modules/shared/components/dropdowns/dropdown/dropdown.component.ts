// ANGULAR DEPENDANCY
import { Component, ContentChild, ElementRef, EventEmitter,  Input,  Output, ViewEncapsulation } from '@angular/core';

// INTERFACES AND ENUMS
import { DropDownConfig, DropDownType } from '../../../../../../app/models/common/forms/dropdown';

/**
 * This component is used for rendering dropdown button
 *
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 04/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>)  dropdown component created
 */
@Component({
  selector: 'kt-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DropdownComponent {
  /**
	  * External inputs
	  */
  @Input() dropdownConfig: DropDownConfig;

  /**
	 * External outputs
	 */
  @Output() dropdownClick = new EventEmitter<any>();

  /**
	 *
	 * Template ref to body template
	 */
  @ContentChild('bodyTemplate', { static: false }) bodyTemplate: any;

  /**
	 * dropdown type config constants
	 */
  dropdownType = DropDownType;
  /**
	 *
	 * Emits event on button click with associated data
	 */
  clickHandler(data) {
    this.dropdownClick.emit(data);
  }


}
