// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

// COMPONENTS
import { DropdownComponent } from './dropdown.component';

/**
 * This component used for generation of different types of buttons
 *
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 04/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>) button component created
 */

@NgModule({
  declarations: [DropdownComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************External MODULES*******************/
    NgbModule,

  ],
  exports: [
    DropdownComponent,
  ],
})
export class DropDownModule { }
