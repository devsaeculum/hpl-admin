// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// MODALS MODULES
import { BaseModalModule } from './base-modal/base-modal.module';
import { NotificationModule } from './notification/notification.module';

/**
 * This module used for all modals configuration
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) all modal module created
 */
@NgModule({
  declarations: [],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************MODALS MODULES*******************/
    BaseModalModule,
    NotificationModule,
  ], exports: [
    /********************EXPORT MODAL MODULES*******************/
    BaseModalModule,
    NotificationModule,
  ],
})
export class ModalsModule { }
