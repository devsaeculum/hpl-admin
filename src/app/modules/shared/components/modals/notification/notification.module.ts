// ANGULAR DEPEDENCY
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// NGB DEPENDENCY
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

// COMPONENTS
import { TranslateModule } from '@ngx-translate/core';
import { NotificationComponent } from './notification.component';

/**
 * This module used for modal creation
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) module created
 */
@NgModule({
  declarations: [NotificationComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************External MODULES*******************/
    NgbModalModule,
    TranslateModule.forChild(),
  ],
  exports: [
    NotificationComponent,
  ],
  entryComponents: [NotificationComponent],
})
export class NotificationModule { }
