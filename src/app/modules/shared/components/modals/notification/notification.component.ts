// ANGULAR DEPENDENCY
import { Component, Input } from '@angular/core';

// NGB DEPENDENCY
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

// INTERFACES AND ENUMS
import { ModalButtonType, ModalColorType, ModalConfig, ModalType } from '../../../../../models/common/modal/modal-config';

/**
 * This component used for notification
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date:8/04/2020 (Pratik Shihora <pratik@saeculumsolutions.com>)  component created
 */
@Component({
  selector: 'kt-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent {

  /**
   * External inputs
   */
  _options: ModalConfig;
  get options(): ModalConfig {
    return this._options;
  }

  @Input('options')
  set options(value: ModalConfig) {
    this._options = value;
    if (value) {
      (value.type, ModalColorType);
      this.className = ModalColorType[value.type];
    }
  }

  modalType = ModalType;
  className: string = null;
  constructor(public activeModal: NgbActiveModal) { }

  /**
   *
   *  Called when the close button get clicked in the modal
   */
  close() {
    this.activeModal.close({ STATUS: ModalButtonType.CLOSE });
  }

  /**
   *
   *  Called when the ok button get clicked in the modal
   */
  ok() {
    this.activeModal.close({ STATUS: ModalButtonType.OK });
  }

  /**
   *
   *  Called when the cancel button get clicked in the modal
   */
  cancel() {
    this.activeModal.close({ STATUS: ModalButtonType.CANCEL });
  }

  /**
   *
   *  Called when the delete button get clicked in the modal
   */
  delete() {
    this.activeModal.close({ STATUS: ModalButtonType.DELETE });
  }

}
