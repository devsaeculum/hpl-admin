// ANGULAR
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

// NGB MODAL
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

/**
 * This component used for render modal for with dynamic body and footer
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) base modal component created
 */
@Component({
  selector: 'kt-base-modal',
  templateUrl: './base-modal.component.html',
  styleUrls: ['./base-modal.component.scss'],
})
export class BaseModalComponent implements OnInit {
  /**
   * External inputs
   */
  @Input() title: any;
  @Input() showFooter = false;
  @Input() emitCloseEvent = false;
  @Input() rightTitle = '';
  @Output() eModalClose = new EventEmitter<boolean>();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  /**
   * To handle the unsaved change not closing the modal instead emitting the close event
   */
  closeClickHandler() {
    if (this.emitCloseEvent) {
      this.eModalClose.emit(true);
    } else {
      this.activeModal.close('Close');
    }
  }
}
