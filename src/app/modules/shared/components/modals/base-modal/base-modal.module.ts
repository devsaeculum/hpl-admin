// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// NGB DEPENDENCY
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

// TRANSLATE MODULE
import { TranslateModule } from '@ngx-translate/core';

// COMPONENTS
import { BaseModalComponent } from './base-modal.component';

/**
 * This module used for render modal for with dynamic body and footer
 *
 * @author Pratik Shihora <pratik@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 30/03/2020 (Pratik Shihora <pratik@saeculumsolutions.com>) base modal module created
 */
@NgModule({
  declarations: [BaseModalComponent],
  imports: [
    /********************ANGULAR MODULES*******************/
    CommonModule,
    /********************NGB MODULES*******************/
    NgbModalModule,
   /********************CUSTOM MODULES*******************/
    TranslateModule.forChild(),
  ],
  exports: [BaseModalComponent],
})
export class BaseModalModule { }
