import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HamburgerMenuComponent } from './hamburger-menu.component';
import { TableModule } from '../shared/components/table/table.module';
import { TableSubHeaderModule } from '../shared/components/table-sub-header/table-sub-header.module';
import { HamburgerRoutingModule } from './hamburger-menu-routing.module';

@NgModule({
  declarations: [HamburgerMenuComponent],
  imports: [
    CommonModule,
    TableModule,
    TableSubHeaderModule,
    HamburgerRoutingModule
  ]
})
export class HamburgerMenuModule { }
