import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddHamburgerComponent } from './add-hamburger/add-hamburger.component';
import { HamburgerMenuComponent } from './hamburger-menu.component';

// const routes: Routes = [
//   { path: '', component: UserListComponent },
//   { path: 'add-user', component: AddUserComponent },
// ];

const routes: Routes = [
    { path: '', component: HamburgerMenuComponent },
    { path: 'add-hamburger', component: AddHamburgerComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HamburgerRoutingModule { }
