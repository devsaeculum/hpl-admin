import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OPTION_HAMBURGER_MENU } from './hamburger-menu.constant';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.scss']
})
export class HamburgerMenuComponent implements OnInit {

  optionHanburgerMenu = OPTION_HAMBURGER_MENU;

  constructor(public router: Router) { }

  ngOnInit(): void {
  }

  loaddata(payload) {
    (payload.data.text);
    if (payload.data.text === 'Add New') {
      this.router.navigate(['/hamburger-menu/add-hamburger']);
    }
    else if (payload.data.text === 'Back') {
      this.router.navigate(['/hamburger-menu/add-hamburger']);
    }

  }

}
