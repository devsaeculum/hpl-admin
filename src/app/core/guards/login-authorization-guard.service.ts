import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthStorageService } from '../../modules/services/auth/auth-storage.service';
@Injectable({
    providedIn: 'root'
})
export class LoginAuthorizationGuardService implements CanActivate {

    constructor(
        private router: Router,
        private authenticationService: AuthStorageService
    ) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | boolean {
        return this.checkIfAuthorized(route, state);
    }

    checkIfAuthorized(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | boolean {
        return this.authenticationService.isAuthorized().pipe(map((isAuthorized: any) => {
            if (isAuthorized.isLoggedIn) {
                this.router.navigate(['/user-list']);
                return false;
            }
            return true;
        }));
    }
}
