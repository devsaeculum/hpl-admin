export class MenuConfig {
    public defaults: any = {
        header: {
            self: {},
            items: [
                {
                    title: 'Hamburger-menu',
                    root: true,
                    alignment: 'left',
                    page: '/hamburger-menu',
                },
                {
                    title: 'User',
                    root: true,
                    alignment: 'left',
                    page: '/user-list',
                },
            ]
        }
    }

    public get configs(): any {
        return this.defaults;
    }
}