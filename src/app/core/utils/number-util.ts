

export class NumberUtil {
    /**
       *
       *
       * @static
       * @param {number} value
       * @returns number with padded 0's
       * @memberof NumberUtil
       */
    static padNumber(value: number) {
      if (NumberUtil.isNumber(value)) {
        return `0${value}`.slice(-2);
      } else {
        return '';
      }
    }
  
    /**
     *
     *
     * @static
     * @param {*} value
     * @returns True if the given argument is a number else false
     * @memberof NumberUtil
     */
    static isNumber(value: any) {
      return !isNaN(NumberUtil.toInteger(value));
    }
  
    /**
     *
     *
     * @static
     * @param {*} value
     * @returns {number} returns the argument to number
     * @memberof NumberUtil
     */
    static toInteger(value: any): number {
      return parseInt(`${value}`, 10);
    }
  
  }
  