// ANGULAR DEPENDENCY
import { Injectable } from '@angular/core';

// EXTERNAL DEPENDENCY
import { NgbDate, NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
//import moment from 'moment';
import * as moment from 'moment'
import * as  momentTimeZone from 'moment-timezone';


// UTILS
import { LocalStorageUtil } from './local-storage-util';
import { NumberUtil } from './number-util';

export class DateUtil {

  static setDefaultTimeZone(timeZone = null) {
    // momentTimeZone.tz.setDefault(timeZone);
  }

  /**
   * To convert the date to UTC
   * @param date To convert the given
   */
  static dateToUTC(date) {
    const dateFormat = LocalStorageUtil.getDateFormat();
    const timeZone = LocalStorageUtil.getTimeZone();
    // momentTimeZone.tz.setDefault(timeZone);
    return momentTimeZone(date).utc().format(dateFormat);
  }

  /**
   * To Convert UTC to date
   * @param date
   */
  static utcToDate(date) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    // momentTimeZone.tz.setDefault(timeZone);

    return momentTimeZone.utc(date).tz(timeZone).format(dateFormat);
  }

  static getCurrentDateTime() {
    const timeZone = LocalStorageUtil.getTimeZone();

    return moment.tz(timeZone);
  }

  /**
 * To convert the date to UTC
 * @param date To convert the given
 */
  static convertDateToUTC(date, isToDate = false) {
    const dateFormat = LocalStorageUtil.getDateFormat();
    const timeZone = LocalStorageUtil.getTimeZone();
    // momentTimeZone.tz.setDefault('Australia/Sydney');
    (momentTimeZone(date).format('YYYY-MM-DDTHH:mm:ss'));
    return momentTimeZone(date).add(isToDate ? 23 : 0, 'hour').add(isToDate ? 59 : 0, 'minute').format('YYYY-MM-DDTHH:mm:ss');
  }

  /**
   * To Convert UTC to date
   * @param date
   */
  static convertUtcToDate(date) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();

    return momentTimeZone(date, 'YYYY-MM-DDTHH:mm:ss');
  }

  /**
   * To convert ngbdateto utc
   * @param date
   */
  static convertNgbDateToUtc(date: NgbDate, isToDate = false) {
    const dateObject = DateUtil.transformToDate(date);
    return momentTimeZone(dateObject).add(isToDate ? 23 : 0, 'hour').add(isToDate ? 59 : 0, 'minute').format('YYYY-MM-DDTHH:mm:ss');
  }


  /**
   * To Convert UTC to date
   * @param date
  */
  static utcToDateTime(date) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    const timeFormat = LocalStorageUtil.getTimeFormat();
    return momentTimeZone.utc(date).tz(timeZone).format(`${dateFormat} ${timeFormat}`);
  }

  /**
  * To Convert UTC to date
  * @param date
  */
  static utcToNgbDate(date) {
    if (!date) return null;
    const timeZone = LocalStorageUtil.getTimeZone();
    // momentTimeZone.tz.setDefault(timeZone);
    const momentDate = momentTimeZone(date);
    return { year: momentDate.year(), month: momentDate.month() + 1, day: momentDate.date() };
  }

  /**
   * To convert the ngbdate to utc
   * @param value
   */
  static ngbDateToUtc(value: NgbDate) {
    const timeZone = LocalStorageUtil.getTimeZone();
    // momentTimeZone.tz.setDefault(timeZone);
    return momentTimeZone({ year: value.year, month: value.month - 1, date: value.day }).format('YYYY-MM-DDTHH:mm:ss');
  }

  /**
   * To convert utc date to timezone date without time
   * @param {string} value
   * @returns {string} date in string
   */
  static tranformToTimeZone(value) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    return momentTimeZone(value, 'YYYY-MM-DDTHH:mm:ss').format(dateFormat);
  }

  static timeFromNow(value) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    return momentTimeZone(value, 'YYYY-MM-DDTHH:mm:ss').fromNow();
  }

  /**
   * To convert utc date to timezone date with time
   * @param {string} value
   * @returns {string} date in string
   */
  static tranformToTimeZoneWithDateTime(value) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    const timeFormat = LocalStorageUtil.getTimeFormat();
    return momentTimeZone(value, 'YYYY-MM-DDTHH:mm:ss').format(`${dateFormat} ${timeFormat}`);
  }

  static ngbDateToUtcForFilters(value: NgbDate) {
    const startDate = momentTimeZone({ year: value.year, month: value.month - 1, date: value.day }).format('YYYY-MM-DDTHH:mm:ss');
    const endDate = momentTimeZone({ year: value.year, month: value.month - 1, date: value.day }).add(1, 'days').subtract(1, 'minutes').format('YYYY-MM-DDTHH:mm:ss');
    return { startDate, endDate };
  }



  /**
   * To convert utc date to timezone date with time
   * @param {string} value
   * @returns {string} date in string
   */
  static tranformToTimeZoneWithTime(value) {
    const timeZone = LocalStorageUtil.getTimeZone();
    const dateFormat = LocalStorageUtil.getDateFormat();
    const timeFormat = LocalStorageUtil.getTimeFormat();

    return momentTimeZone(value, 'YYYY-MM-DDTHH:mm:ss').format(`${timeFormat}`);
  }

  /**
  * Used to transform the Date Object to the  NgbDate object
  * @param date
  * @returns { year, month, date } format
  */
  static parse(momentDate: momentTimeZone.Moment | Date) {
    momentDate = moment(momentDate, 'YYYY-MM-DDTHH:mm:ss');
    let returnDate = null;
    if (momentDate) {
      //  const date = momentDate.toDate();
      const day = +momentDate.format('DD');
      const month = ((+momentDate.format('MM')));
      const year = +momentDate.format('YYYY');
      returnDate = { year, month, day };
    }
    return returnDate;
  }

  /**
   * @static
   * @param {NgbDate} value
   * @memberof DateUtil
   */
  static transformToDate(evt: any) {
    return moment({ year: evt.year, month: evt.month - 1, date: evt.day });
  }

  /**
   * Used to transform the ngbdate to the Date object
   * @param {NgbDate} value
   * @returns {Date}
   */
  static transform(value: NgbDate): string {
    return moment({ year: value.year, month: value.month - 1, date: value.day }).format();
  }

  /**
  * Used to transform the ngbdate to the Date object
  * @param {NgbDate} value
  * @returns {Date}
  */
  static transformDate(value: NgbDate): momentTimeZone.Moment {
    const timeZone = LocalStorageUtil.getTimeZone();
    // momentTimeZone.tz.setDefault(timeZone);
    return momentTimeZone({ year: value.year, month: value.month - 1, date: value.day });
  }

  /**
   * Used to transform the ngbdate to the Date object
   * @param {NgbDate} value
   * @returns {Date}
   */
  static convert(value): string {
    return moment(value).format();
  }

  /**
   * Used to transform the backend date to ddmmyyyy format
   */
  static convertToOnlyDate(value) {
    const date = new Date(value);
    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
  }

  static dateDifference(now, then) {
    const diff: any = moment.duration(moment(new Date(then)).diff(moment(new Date(now))));
    // tslint:disable-next-line: radix
    const days = parseInt(diff.asDays());
    // tslint:disable-next-line: radix
    let hours = parseInt(diff.asHours());
    hours = hours - days * 24;
    // tslint:disable-next-line: radix
    let minutes = parseInt(diff.asMinutes());
    minutes = minutes - (days * 24 * 60 + hours * 60);

    return { days, hours: ('0' + hours).slice(-2), minutes: ('0' + minutes).slice(-2) };
  }

  static getDay(date) {
    return (new Date(date)).getDay();
  }

  /**
   * Convert time into milliseconds
   * @param time
   */
  static getTimeToSeconds(time) {
    const hoursMinutes = time.split(':');
    const hourSeconds = (+hoursMinutes[0]) * 60 * 60;
    const minutesSeconds = (+hoursMinutes[1]) * 60;

    return hourSeconds + minutesSeconds;

  }

  /**
   * Convert time into minutes
   * @param time
   */
  static getTimeToMinutes(time) {
    const hoursMinutes = time.split(':');
    const hourSeconds = (+hoursMinutes[0]) * 60;
    const minutesSeconds = (+hoursMinutes[1]);

    return hourSeconds + minutesSeconds;

  }

  /**
   * difference between 12:13 to 12: 49 in minutes
   * @param startTime
   * @param endtime
   */
  static differenceBetweenTimeInMinues(startTime, endtime) {
    return Math.abs(DateUtil.getTimeToMinutes(endtime) - DateUtil.getTimeToMinutes(startTime));
  }

  /**
   * add minutes to 12: 45 (45) minutes
   * @param time
   * @param minutesToAdd
   */
  static addMinutesToTime(time, minutesToAdd) {
    const totalMinutes = DateUtil.getTimeToMinutes(time);
    return DateUtil.convertMinutesToTimeString(totalMinutes + minutesToAdd);
  }

  /**
   * Convert Minutes to Time stirn gin HH:: MM format
   */
  static convertMinutesToTimeString(minutesToConvert) {
    let totalMinutes = +minutesToConvert;
    if (totalMinutes > 24 * 60) {
      totalMinutes = totalMinutes - 24 * 60;
    }
    let hours: any = Math.floor(totalMinutes / 60);
    if (hours.toString().length === 1) {
      hours = '0' + hours;
    }
    let minutes: any = totalMinutes - (hours * 60);
    if (minutes.toString().length === 1) {
      minutes = '0' + minutes;
    }

    return `${hours}:${minutes}`;
  }

  /**
   * To split the date and time
   * @param dateTime
   */
  static splitDateTime(dateTime: momentTimeZone.Moment) {
    // momentTimeZone.tz.setDefault(null);
    const onlyDate = moment(dateTime.format('YYYY-MM-DD'), 'YYYY-MM-DD');

    return {
      time: moment(dateTime).format('HH:mm'),
      date: onlyDate,
    }
  }

  /**
   * To combine the date and time
   * @param date
   * @param time
   */
  static combineDateTime(date: NgbDate | Date, time: string, isNgbDate = true) {
    let dateObject: Date | NgbDate | momentTimeZone.Moment = date;
    if (isNgbDate) {
      dateObject = DateUtil.transformToDate(date);
    }

    if (!time) {
      return dateObject;
    }
    const hoursMinutes = time.split(':');

    return moment(dateObject).add(hoursMinutes[0], 'hour').add(hoursMinutes[1], 'minute').toDate();
  }

//   static getDayNumberByDayName(dayName) {

//     switch (dayName) {
//       case 'Monday':
//         return 1;
//       case 'Tuesday':
//         return 2;
//       case 'Wednesday':
//         return 3;
//       case 'Thursday':
//         return 4;
//       case 'Friday':
//         return 5;
//       case 'Saturday':
//         return 6;
//       case 'Sunday':
//         return 7;

//     }
//   }

//   static getDayNameByDayNumber(dayNumber) {
//     switch (dayNumber) {
//       case 1:
//         return 'monday';
//       case 2:
//         return 'tuesday';
//       case 3:
//         return 'wednesday';
//       case 4:
//         return 'thursday';
//       case 5:
//         return 'friday';
//       case 6:
//         return 'saturday';
//       case 7:
//         return 'sunday';

//     }
//   }
}

/**
 *  This class extends NgbDateParserFormatter to override the method parse and format to
 *  convert the default format 'yyyy-mm-dd' to 'dd-mm-yyyy'
 * @class NgbDateFRParserFormatter
 * @extends {NgbDateParserFormatter}
 */
@Injectable()
export class NgbDateFRParserFormatter extends NgbDateParserFormatter {
  parse(value: string): NgbDateStruct {
    if (value) {
      const dateParts = value.trim().split('/');
      if (dateParts.length === 1 && NumberUtil.isNumber(dateParts[0])) {
        return { year: NumberUtil.toInteger(dateParts[0]), month: null, day: null };
      } else if (dateParts.length === 2 && NumberUtil.isNumber(dateParts[0]) && NumberUtil.isNumber(dateParts[1])) {
        return { year: NumberUtil.toInteger(dateParts[1]), month: NumberUtil.toInteger(dateParts[0]), day: null };
      } else if (dateParts.length === 3 && NumberUtil.isNumber(dateParts[0]) && NumberUtil.isNumber(dateParts[1]) && NumberUtil.isNumber(dateParts[2])) {
        return { year: NumberUtil.toInteger(dateParts[2]), month: NumberUtil.toInteger(dateParts[1]), day: NumberUtil.toInteger(dateParts[0]) };
      }
    }

    return null;
  }

  format(date: NgbDateStruct): string {
    let stringDate = '';
    if (date) {
      stringDate += NumberUtil.isNumber(date.day) ? NumberUtil.padNumber(date.day) + '/' : '';
      stringDate += NumberUtil.isNumber(date.month) ? NumberUtil.padNumber(date.month) + '/' : '';
      stringDate += date.year;
    }

    return stringDate;
  }
}
