import { DefaultConstant } from '../../constants/enum/common/default.enum';
import { Role } from '../../constants/enum/common/role-enum';

export class LocalStorageUtil {

  /**
   * To get the fimeformat of the user
   */
  static getTimeFormat() {
    const timeFormat = (localStorage.getItem('business') ? JSON.parse(localStorage.getItem('business')).business_time_format : null);
    let momentTimeformat = DefaultConstant.TIME_12_FORMAT;
    if (timeFormat) {
      if ((+timeFormat) === 12) {
        momentTimeformat = DefaultConstant.TIME_12_FORMAT;
      }
      if ((+timeFormat) === 24) {
        momentTimeformat = DefaultConstant.TIME_24_FORMAT;
      }
    }

    return momentTimeformat;
  }

  /**
   * To get the preferred dateFormat of the user set by the user
   */
  static getDateFormat() {
    // return (localStorage.getItem('dateFormat') || DefaultConstant.DATE_FORMAT);
    return ((localStorage.getItem('business') ? JSON.parse(localStorage.getItem('business')).business_date_format : null) || DefaultConstant.DATE_FORMAT);

  }

  /**
   * To get the timezone of the user
   */
  static getTimeZone() {
    return ((localStorage.getItem('business') && JSON.parse(localStorage.getItem('business')) ? JSON.parse(localStorage.getItem('business')).business_fk_timezone_name : null) || DefaultConstant.TIMEZONE);
  }
  /**
   * To get the timezone of the user
   */
  static getBusinessName() {
    return ((localStorage.getItem('business') ? JSON.parse(localStorage.getItem('business')).business_name : null) || '');
  }

  /**
     * To get the timezone of the user
     */
  static getBusinessTimeFormat() {
    return ((localStorage.getItem('business') ? JSON.parse(localStorage.getItem('business')).business_time_format : 0) || 0);
  }

  static getBusinessId() {
    return ((localStorage.getItem('business') &&  JSON.parse(localStorage.getItem('business')) ? JSON.parse(localStorage.getItem('business')).business_id : 0) || 0);
  }
  static getEmployeeId() {
    return ((localStorage.getItem('business') ? JSON.parse(localStorage.getItem('business')).employee_id : 0) || 0);
  }

  static getUserId() {
    return ((localStorage.getItem('user') && JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')).user_id : 0) || 0);

  }

  static getUserRole() {
    return ((localStorage.getItem('business') && JSON.parse(localStorage.getItem('business')).user_role ? JSON.parse(localStorage.getItem('business')).user_role : Role.Employee) || Role.Employee);
  }

  static getBusinessCurrency() {
    return localStorage.getItem('business') && JSON.parse(localStorage.getItem('business')).currency ? JSON.parse(localStorage.getItem('business')).currency.currency_symbol : '$';
  }
} 