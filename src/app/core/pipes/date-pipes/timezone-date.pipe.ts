// ANGULAR
import { Pipe, PipeTransform } from '@angular/core';

// UTILS
import { DateUtil } from '../../utils/date-util';

@Pipe({ name: 'TimezoneDate' })
export class TimezoneDate implements PipeTransform {

  /**
   * Tp transform the ngbDate to date to be shown in User Interface;
   * @param value
   */
  transform(value: string) {
    const transFormedDate = DateUtil.tranformToTimeZone(value);
    if (transFormedDate === 'Invalid date') {
      return value;
    }

    return transFormedDate;
  }
}
