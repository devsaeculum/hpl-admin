// Angular
import { Pipe, PipeTransform } from '@angular/core';

// Utils
import { DateUtil } from '../../utils/date-util';

@Pipe({ name: 'TimezoneTime' })
export class TimezoneTime implements PipeTransform {

  /**
   * Tp transform the ngbDate to date to be shown in User Interface;
   * @param value
   */
  transform(value: string) {
    const transFormedDate = DateUtil.tranformToTimeZoneWithTime(value);
    if (transFormedDate === 'Invalid date') {
      return value;
    }
    return transFormedDate;
  }
}
