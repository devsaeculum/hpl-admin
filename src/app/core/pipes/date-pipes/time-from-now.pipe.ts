// ANGULAR
import { Pipe, PipeTransform } from '@angular/core';

// UTILS
import { DateUtil } from '../../utils/date-util';

@Pipe({ name: 'TimeFromNow' })
export class TimeFromNow implements PipeTransform {

  /**
   * Tp transform the ngbDate to date to be shown in User Interface;
   * @param value
   */
  transform(value: string) {
    const transFormedDate = DateUtil.timeFromNow(value);
    if (transFormedDate === 'Invalid date') {
      return value;
    }

    return transFormedDate;
  }
}
