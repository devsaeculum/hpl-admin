import { Pipe, PipeTransform } from '@angular/core';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateFRParserFormatter } from '../../utils/date-util';

@Pipe({ name: 'CustomDate' })
export class CustomDate implements PipeTransform {
  transform(value: NgbDate): string {
    return new NgbDateFRParserFormatter().format(value);
  }
}
