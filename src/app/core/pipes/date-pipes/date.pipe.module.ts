// ANGULAR
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

// Pipes
import { CustomDate } from './date.pipe';
import { TimeFromNow } from './time-from-now.pipe';
import { TimezoneDate } from './timezone-date.pipe';
import { TimezoneDateTime } from './timezone-datetime.pipe';
import { TimezoneTime } from './timezone-time.pipe';


/**
 * This component used for Pipe
 * @author Vikas thakkar <vikasthakkar@saeculumsolutions.com>
 *
 * Notes:-
 * Date: 11/04/2020 (Vikas thakkar <vikasthakkar@saeculumsolutions.com>) Date Pipe created
 */
@NgModule({
  declarations: [CustomDate, TimezoneDate,
     TimezoneDateTime, 
     TimezoneTime, TimeFromNow],
  imports: [
    CommonModule,
  ],
  exports: [
    CustomDate,
    TimezoneDate,
    TimezoneDateTime,
    TimezoneTime,
    TimeFromNow,
  ],
})
export class DatePipeModule { }
