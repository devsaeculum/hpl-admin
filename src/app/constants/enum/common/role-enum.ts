export enum Role {
    System_Administrator = 'System Administrator',
    Location_Manager	= 'Location Manager',
    Supervisor	= 'Supervisor',
    Employee = 'Employee',
    Client = 'Client',
  }