
/**
 * Default Constant enum for
 */
export enum DefaultConstant {
  TIMEZONE = 'America/New_York',
  DATE_FORMAT = 'DD/MM/YYYY',
  TIME_12_FORMAT = 'hh:mm:ss a',
  TIME_24_FORMAT = 'HH:mm:ss',
  IMAGE = 'default.jpg',
  OPEN_SHIFT_IMAGE = 'https://iqcheckpoint.s3.ap-south-1.amazonaws.com/OpenShift.jpg',
  EMPTY_SHIFT_IMAGE = 'https://iqcheckpoint.s3.ap-south-1.amazonaws.com/EmptyShift.jpg',
  LOCATION_ICON_URL = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
}