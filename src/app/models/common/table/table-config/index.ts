////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////

import { ButtonConfig } from '../../forms/button';
import { DropDownOption, DropDownConfig } from '../../forms/dropdown';
import { TemplateDrivenField } from '../../forms/template-driven';
import { VisibleOption } from '../../forms/form-field-config';

/**
 * Table Column interface for table config
 */
export interface TableConfig {
  // columns
  columns?: TableColumn[];
  dataKey: string;

  // toggle options
  toggleOptions?: ToggleOption;
  // filter options
  filterable?: boolean;
  expandFilterEnable?: boolean;
  // sort options
  sortable?: boolean;
  sortMode?: ModeType;
  customSort?: boolean;
  sortField?: string;
  sortOrder?: string;
  // redorder options
  reorderableColumns?: boolean;
  reorderableRows?: boolean;
  // responsive options
  responsive?: boolean;
  // selections options
  selectionMode?: ModeType;
  metaKeySelection?: boolean;
  // scroll options
  scrollable?: boolean;
  scrollHeight?: string;
  fullPageHeight?: boolean;

  // frozen options
  frozenWidth?: string;
  frozenColumns?: TableColumn[];
  // resize options
  resizableColumns?: boolean;
  columnResizeMode?: ResizeType;
  // setting storage options
  stateStorage?: StorageType;
  stateKey?: string;
  // paging options
  paginator?: boolean;
  rows?: number;
  rowsPerPageOptions?: number[],
  lazy?: boolean;
  // filter options

  expandable?: boolean;
  // export options
  exportEnable?: boolean;
  exportOptions?: TableExportOption;
  exportFileName?: string;
  checkboxSelection?: boolean;
  radioSelection?: boolean;
  footerVisible?: boolean;
  // styles
  style?: string;
  sticky?: boolean;

  // header options
  header?: HeaderOption;

  actionButtonOptions?: ActionButtonOption;
  // action options
  actions?: ActionOption;
  multiSortMeta?: Array<any>;
  globalFilterFields?: string[];

  bulkOptions?: BulkOption;
}

/**
 * Table Column interface for table config
 */
export interface TableColumn {
  field: string;
  header: string;
  sortable?: boolean;
  filterable?: boolean;
  filterType?: FilterType;
  width?: string;
  minWidth?: string;
  defaultWidth?: string;
  filterBy?: FilterByType;
  displayType?: DisplayType;
  setting?: TemplateDrivenField;
  dateTimePipe?: boolean;
  options?: FilterSelectOption[] | FilterRangeOption | FilterDateOption;
}

/**
 * Table Column interface for table config
 */
export interface ToggleOption {
  show?: boolean;
  columns?: ToggleColumn[];
}

/**
 * Table Column interface for table config
 */
export interface ToggleColumn {
  field: string;
  header?: string;
  selected?: boolean;
}

/**
 * Filter Select Option interface for filter config
 */
export interface FilterSelectOption {
  id: string;
  name: string;
}

/**
 * Filter Range Option interface for filter config
 */
export interface FilterRangeOption {
  min?: number;
  max?: number;
  custom?: boolean;
}

/**
 * Filter Date Option interface for filter config
 */
export interface FilterDateOption {
  min?: string;
  max?: string;
}

/**
 * Export Option interface for export config
 */
export interface TableExportOption {
  show?: boolean;
  csv?: ExportOption;
  excel?: ExportOption;
  pdf?: ExportOption;
  permission?: any;
  }

/**
 * Export Option interface for export config
 */
export interface ActionButtonOption {
  show?: boolean;
  showFunction?: (rowData: any) => boolean;
  title: string;
  width?: string;
  minWidth?: string;
  defaultWidth?: string;
  buttons?: ButtonConfig[];
}

/**
 * Export Option interface for export config
 */
export interface HeaderOption {
  show?: boolean;
  title?: string;
  columnFilter?: boolean;
  globalFilter?: boolean;
  globalFilterFields?: string[];
  buttons?: ButtonConfig[]
  dropdowns?: DropDownConfig[];
}

/**
 * Action Option interface for action config
 */
export interface ActionOption {
  show: boolean;
  status?: string;
  edit?: boolean;
  view?: boolean;
  delete?: boolean;
  width?: string;
  minWidth?: string;
  defaultWidth?: string;
  setting?: boolean;
  permission?: any;
  deleteVisibleExpression?: VisibleOption;
  editVisibleExpression?: VisibleOption;
  statusConfirmation?: boolean;
  editConfirmation?: boolean;
  deleteConfirmation?: boolean;
  settingConfirmation?: boolean;
}

/**
 * Table Column interface for table config
 */
export interface BulkOption {
  show?: boolean;
  options?: DropDownOption[],
  buttons?: ButtonConfig[]
}

/**
 * Table Column interface for table config
 */
export interface ExportOption {
  show?: boolean;
  type?: ExportType,
  action?: ExportActionType
}

////////////////////////////////////////
//              ENUMS                 //
////////////////////////////////////////
/**
 * Filter Type enum for filter type config
 */
export enum FilterType {
  DATE = 'date',
  STRING = 'string',
  MULTI_SELECT = 'multiselect',
  SELECT = 'select',
  NUMBER = 'number',
  RANGE = 'range',
}

/**
 * Filter By Type enum for filter by type config
 */
export enum FilterByType {
  EQUAL = 'eq',
  IN = 'in',
  CUSTOM = 'custom',
  CONTAIN = 'contains',
  GT = 'gt',
  LT = 'lt',
}

/**
 * Mode Type enum for selection and sort config
 */
export enum ModeType {
  SINGLE = 'single',
  MULTIPLE = 'multiple',
}

/**
 * Resize Type enum for selection and sort config
 */
export enum ResizeType {
  EXPAND = 'expand',
  FIT = 'fit',
  NONE = 'none',
}

/**
 * Storage Type enum for selection and sort config
 */
export enum StorageType {
  SESSION = 'session',
  LOCAL = 'local'
}

/**
 * Export Type enum for export config
 */
export enum ExportType {
  INBUILT = 'inbuilt',
  CUSTOM = 'custom',
  NONE = 'none'
}

/**
 * Action Type enum for export config
 */
export enum ActionType {
  EDIT = 'edit',
  DELETE = 'delete',
  SETTING = 'setting',
  STATUS = 'status',
}

/**
 * Export Action Type enum for export config
 */
export enum ExportActionType {
  EXPORT_PDF = 'pdf',
  EXPORT_CSV = 'csv',
  EXPORT_EXCEL = 'excel',
}

/**
 * Bulk Action Type enum for export config
 */
export enum BulkActionType {
  DELETE_SELECTED = 'delete selected',
  UPDATE_STATUS = 'update status'
}

/**
 * Export Type enum for export config
 */
export enum TableEventType {
  SORT = 'sort',
  ACTION = 'action',
  SELECT = 'select',
  UN_SELECT = 'unselect',
  CUSTOM_SORT = 'custom_sort',
  LAZY = 'lazy',
  EXPAND = 'expand',
  COLLAPSE = 'collapse',
  PAGE = 'page',
  FILTER = 'filter',
  CONTEXT_MENU = 'context_menu',
  RESIZE = 'resize',
  COL_REORDER = 'col_reorder',
  ROW_REORDER = 'row_reorder',
  TOGGLE = 'toggle',
  FIRST = 'first',
  ROW = 'row',
  STATE_SAVE = 'state_save',
  STATE_RESTORE = 'state_restore',
  EXPORT = 'export',
  EXPORT_PDF = 'export_pdf',
  EXPORT_CSV = 'export_csv',
  EXPORT_EXCEL = 'export_excel',
  EDIT = 'edit',
  VIEW = 'view',
  DELETE = 'delete'
}

export enum DisplayType {
  SWITCH = 'switch',
  CLICKABLE = 'clickable',
  DATE = 'date',
  DATETIME = 'datetime',
  IMAGE = 'image',
  CUSTOM = 'custom'
}