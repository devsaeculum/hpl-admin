////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////
/**
 * Modal Config interface for notification/confirmation modal
 */
export interface ModalConfig {
    type: ModalType;
    title: string;
    message: string;
    messageHighlight?: string;
    buttons?: ModalButton;
    code?: string;
  }
  
  /**
   * Modal Button interface for buttons
   */
  export interface ModalButton {
    // Delete or Ok
    button1: ModalButtonOption;
    // Cancel
    button2?: ModalButtonOption;
    // button3
    button3?: ModalButtonOption;
  }
  
  /**
   * Modal Button Option interface for button config
   */
  export interface ModalButtonOption {
    text?: string;
    icon?: string;
    className?: string;
  }
  
  ////////////////////////////////////////
  //              ENUMS                 //
  ////////////////////////////////////////
  /**
   * Modal type enum for notification and confirmation modal
   */
  export enum ModalType {
    ALERT = 'alert',
    WARNING = 'warning',
    DELETE = 'danger',
    INFO = 'info',
    SUCCESS = 'success',
    SUCCESS_CONFIRM = 'success_confirm',
    ERROR = 'error',
    ENABLE = 'enable',
    DISABLE = 'disable',
  }
  
  export enum ModalColorType {
    alert = 'alert',
    warning = 'warning',
    danger = 'danger',
    info = 'info',
    success_confirm = 'success',
    success = 'success',
    error = 'danger',
  }
  
  /**
   * Modal button type enum for handle button click event
   */
  export enum ModalButtonType {
    CLOSE = 'close',
    CANCEL = 'cancel',
    OK = 'ok',
    DELETE = 'delete',
    OK_ADD = 'ok_add',
  }
  