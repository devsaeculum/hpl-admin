////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////
/**
 * Form Field interface for fields
 */


export interface FormFieldConfig {
    row: number;
    fields: Field[];
  }
  
  /**
   * Field interface for field config
   */
  export interface Field {
    key: string;
    type: ControlType;
    component: string;
    templateOptions: TemplateOption;
    validations?: Validation;
    errorMessages?: ErrorMessage;
    classNames?: ClassName;
    visibleExpression?: VisibleOption;
    permission?: string;
    options?: Option[];
    //   temp_path?: string;
  }
  
  /**
   * Template option interface for field config
   */
  export interface TemplateOption {
    label?: string;
    withoutLabel?: boolean;
    placeholder?: string;
    description?: string;
    layout?: Layout;
    tooltip?: string;
    hint?: string;
    labelHint?: string;
    icon?: Icon;
    disabled?: boolean;
    multiple?: boolean;
    bindValue?: string;
    bindLabel?: string;
    layoutOptions?: LayoutOptions;
    selectOptions?: SelectOption;
    uploadKey?: string;
    // path where api will search
    typeAhead?: string;
    // path at which data  is present
    typeAheadPath?: string;
    searchAsync?: boolean;
    accepts?: string;
    range?: number;
    position?: Placement;
    locationLatKey?: string;
    locationLongKey?: string;
    mapTitleKey?: string;
    maxFileUpload?: number;
    textAreaRows?: number;
    groupBy?: string;
    selectableGroup?: boolean;
    selectableGroupAsModel?: boolean;
  }
  
  
  export interface SelectOption {
    addTag?: (name: string) => any;
    isOpen?: boolean;
    checkboxSelection?: boolean;
    inputDropDown?: boolean;
    selectAll?: boolean;
    unSelectAll?: boolean;
    closeOnSelect?: boolean;
  }
  
  /**
   * Icon interface for icon type and position
   */
  export interface Icon {
    type?: string;
    right?: IconOption;
    left?: IconOption;
  }
  
  /**
   * Icon option interface for icon config
   */
  export interface IconOption {
    show: boolean;
    icon?: string;
    text?: string;
    click: () => void;
  }
  
  /**
   * Validation interface for validators and messagesF
   */
  export interface Validation {
    validators?: Validators;
    messages?: ErrorMessage;
  }
  
  /**
   * Validator interface for form validation config
   */
  export interface Validators {
    required?: boolean;
    min?: number;
    max?: number;
    min_length?: number;
    max_length?: number;
    pattern?: string | RegExp;
    min_selection?: number;
    max_selection?: number;
    req_selection?: boolean;
    min_date?: any;
    max_date?: any;
    max_date_no_future?: boolean;
    max_date_no_past?: boolean;
    allowedExtensions?: any;
    custom_password?: string;
    from_date_gt_to_date?: string;
    to_date_lt_from_date?: string;
    multi_email?: boolean;
  }
  
  /**
   * Error message interface for validation message config
   */
  export interface ErrorMessage {
    required?: string;
    min?: string;
    max?: string;
    min_length?: string;
    max_length?: string;
    pattern?: string;
    min_selection?: string;
    max_selection?: string;
    req_selection?: string;
    min_date?: string;
    max_date?: string;
    allowedExtensions?: string;
    custom_password?: string;
    from_date_gt_to_date?: string;
    to_date_lt_from_date?: string;
    multi_email?: boolean;
  
  }
  
  /**
   * ClassName interface for form layout class config
   */
  export interface ClassName {
    main?: string;
    label?: string;
    input?: string;
    inputOuter?: string;
    inputInner?: string;
  }
  
  /**
   * Template option interface for field config
   */
  export interface VisibleOption {
    field?: string;
    value?: any;
    key?: string;
    type?: VisibleExpressionType;
  }
  /**
   * Option interface for select, radio config
   */
  export interface Option {
    key: string;
    label: string;
  }
  
  ////////////////////////////////////////
  //              ENUMS                 //
  ////////////////////////////////////////
  /**
   * Layout enum for form layout config
   */
  export enum Layout {
    VERTICAL = 'vertical',
    HORIZONTAL = 'horizontal',
  }
  
  /**
   * Icon type enum for icon config
   */
  export enum IconType {
    INLINE = 'inline',
    GROUP = 'group',
  }
  
  /**
   * Control type enum for form control type config
   */
  export enum ControlType {
    TEXT = 'text',
    EMAIL = 'email',
    URL = 'url',
    TEXTAREA = 'textarea',
    NUMBER = 'number',
    PASSWORD = 'password',
    RADIO = 'radio',
    SELECT = 'select',
    DATE = 'date',
    TIME = 'time',
    CHECKBOX = 'checkbox',
    SWITCH = 'switch',
    WEEKSELECTOR = 'weekselector',
    TEXTEDITOR = 'texteditor',
    MENTIONEDITOR = 'mentioneditor',
    FILEUPLOAD = 'FileUPLOAD',
    PROFILEUPLOAD = 'profileupload',
    COLORPICKER = 'colorpicker',
    AUTOCOMPLETE_ADDRESS = 'autocompleteaddress',
    MULTI_SELECT = 'multiselect',
  }
  
  /**
   * Form component type enum for form components
   */
  export enum FormComponentType {
    TEXTBOX = 'kt-textbox-input',
    CHECKBOX = 'kt-checkbox',
    RADIO = 'kt-radio',
    TEXTAREA = 'kt-textaarea',
    DATEPICKER = 'kt-datepicker',
    SWITCH = 'kt-witch',
    SELECT = 'kt-select',
    WEEKSELECTOR = 'kt-week-selector',
    TEXTEDITOR = 'KT-text-edior',
    MENTIONEDITOR = 'kt-mention-edior',
    PROFILEUPLOAD = 'kt-profile-upload',
    FILEUPLOAD = 'kt-file-upload',
    COLORPICKER = 'kt-color-picker',
    AUTOCOMPLETE_ADDRESS = 'kt-autocomplete-address',
    MULTI_SELECT = 'kt-multi-select',
  }
  
  /**
   *  Layout options for checkbox components
   */
  export enum LayoutOptions {
    Inline = 'inline',
    List = 'list',
  }
  
  /**
   *
   * possible values for the placement
   */
  export enum Placement {
    Top = 'top',
    TopLeft = 'top-left',
    TopRight = 'top-right',
    Bottom = 'bottom',
    BottomLeft = 'bottom-left',
    BottomRight = 'bottom-right',
    Left = 'left',
    Right = 'right',
  }
  
  /**
   *  Layout options for checkbox components
   */
  export enum VisibleExpressionType {
    AnyOf = 'any-of',
    Array = 'array',
    ArrayWithKey = 'array-with-key',
    Object = 'object'
  }
  