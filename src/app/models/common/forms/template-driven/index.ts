////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////

import { ControlType, TemplateOption } from '../form-field-config';

/**
 * Field interface for field config
 */
export interface TemplateDrivenField {
  key: string;
  type: ControlType;
  component: string;
  templateOptions: TemplateOption;
}

////////////////////////////////////////
//              ENUMS                 //
////////////////////////////////////////
/**
 * Form component type enum for form components
 */
export enum TemplateFormComponentType {
  TEXTBOX = 'kt-sa-textbox',
  DATEPICKER = 'kt-sa-datepicker',
  SELECT = 'kt-sa-select',
}