////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////

import { ModalType } from '../../modal/modal-config';
import { VisibleOption } from '../form-field-config';

/**
 * Form Field interface for fields
 */
export interface ButtonConfig {
  options?: ButtonOption ;
  type: ButtonConfigType;
  visibleExpression?: VisibleOption;
  permission?: string;
  tooltip?: string;
}

/**
 * Button option interface for button config
 */
export interface ButtonOption {
  type: ButtonType;
  left?: ButtonIcon;
  right?: ButtonIcon;
  className: string;
  loadingClass?: string;
  text: string;
  action?: ActionButtonType;
  confirmation?: ConfirmationOption;
}

/**
 * Button Icon properties for button left and right icon
*/
export interface ButtonIcon {
  show: boolean;
  icon: string;
}

/**
 * Button Icon properties for button left and right icon
*/
export interface ConfirmationOption {
  show: boolean;
  message: string;
  type: ModalType,
  title: string,
}

////////////////////////////////////////
//              ENUMS                 //
////////////////////////////////////////

/**
 * Button Type properties for the button components
*/
export enum ButtonType {
  Submit = 'submit',
  button = 'button',
}

/**
 * Single or multiple buttons types
*/
export enum ButtonConfigType {
  Group = 'group',
  Single = 'single',
}

/**
 * Single or multiple buttons types
*/
export enum ActionButtonType {
  Edit = 'edit',
  Back = 'back',
  Add = 'add',
  Approve = 'Approve',
  Reject = 'Reject',
  Unapprove = 'Unapprove',
  View = 'View',
  Send_Invite = 'Send_Invite',
  View_Qr_Code = 'View_Qr_Code',
  View_Nfc = 'View_Nfc',
  View_Beacon = 'View_Beacon',
  Quick_Add = 'Qucik_Add',
  Delete = 'delete',
  Approve_Selected = 'Approve selected',
  Reject_Selected = 'Reject selected',
  FILL = 'FILL',
}

