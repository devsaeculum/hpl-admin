////////////////////////////////////////
//            INTERFACES              //
////////////////////////////////////////

import { ConfirmationOption } from '../button';

/**
 * Dropdownconfig interface for the dropdown config
 */
export interface DropDownConfig {
  classNames: ClassName;
  type: DropDownType;
  showHeader: boolean;
  showFooter: boolean;
  showUnToogle?: boolean;
  showUnToggle?: boolean;
  text?: string;
  toggleOptions?: ToggleOption;
  placement: Placement;
  options: DropDownOption[];
}

/**
 * ClassName interface for the dropdown class
 */
export interface ClassName {
  dropdown?: string;
  dropdownToggle?: string;
  dropdownUnToggle?: string;
  dropdownMenu?: string;
}

/**
 * Toggle Option interface for the toggle section
 */
export interface ToggleOption {
  icon?: string;
  unToggleIcon?: string;
  toggleText?: string;
  unToggleText?: string;
}

/**
 * Interface for the option in the dorpdown
 */
export interface DropDownOption {
  id?: number;
  text: string;
  left?: IconOption;
  right?: IconOption;
  seperator: boolean;
  badge?: BadgeOption;
  confirmation?: ConfirmationOption;
  action?: string;
}

/**
 * Icon option interface for icon config
 */
export interface IconOption {
  show: boolean;
  icon: string;
}

/**
 * Badge option interface for badge config
 */
export interface BadgeOption {
  text?: string;
  className?: string;
}

////////////////////////////////////////
//              ENUMS                 //
////////////////////////////////////////

/**
 * dropdown type
 */
export enum DropDownType {
  Default = 'default',
  Split = 'split',
}


/**
 * possible values for the placement
 */
export enum Placement {
  Top = 'top',
  TopLeft = 'top-left',
  TopRight = 'top-right',
  Bottom = 'bottom',
  BottomLeft = 'bottom-left',
  BottomRight = 'bottom-right',
  Left = 'left',
  LeftTop = 'left-top',
  LeftBottom = 'left-bottom',
  Right = 'right',
  RightTop = 'right-top',
  RightBottom = 'right-bottom',
}
