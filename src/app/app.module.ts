import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TranslateModule } from '@ngx-translate/core';

// NGX PERMISSIONS
import { NgxPermissionsModule, NgxPermissionsService } from 'ngx-permissions';

import { LayoutModule } from './modules/layout/layout.module';
import { StateStoreModule } from './state/state.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TranslateModule.forRoot(),
    NgxPermissionsModule.forRoot(),
    LayoutModule,
    StateStoreModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
